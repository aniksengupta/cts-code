import java.io.Serializable;

public class Employee implements Serializable{
	
	String name;
	String roll;
	
	public Employee(String name,String roll) {
		this.name = name;
		this.roll = roll;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoll() {
		return roll;
	}

	public void setRoll(String roll) {
		this.roll = roll;
	}

	public void Show() {
		System.out.println("show method = "+this.name+" -- "+this.roll);
	}
}
