import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

public class Main {
	public static void main(String args[]) throws IOException, ClassNotFoundException {
		
		Employee e1 = new Employee("Anik","1");
		Employee e2 = new Employee("Smaranika","2");
		
		File f = new File("employee.txt");
		
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(e1);
		oos.writeObject(e2);
		
		
		FileInputStream fis = new FileInputStream(f);
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		while(fis.available()>0) {
			Employee e = (Employee)ois.readObject();
			System.out.println("val = "+e.getName()+" - "+e.getRoll());
			e.Show();
		}
		
		fos.close();
		fis.close();
		
		List<String> listOfString = new ArrayList<>();
		listOfString.add("a");
		Set<String> setOfString = new HashSet<>();
		setOfString.add("s");
		Vector v = new Vector();
		v.add(1);
		System.out.println("list size = "+listOfString.size());
		System.out.println("set size = "+setOfString.size());
		System.out.println("vector size = "+v.size());
	}
}
