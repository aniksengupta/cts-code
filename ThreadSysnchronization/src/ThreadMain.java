
public class ThreadMain {

	int i=0;
	void increment() {
		i++;
	}
	
	public static void main(String args[]) throws InterruptedException {
		ThreadMain obj = new ThreadMain();
		Thread t1 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				for(int i=0;i<=10000;i++) {
					obj.increment();
					}
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				for(int i=0;i<=10000;i++) {
				obj.increment();
				}
			}
		});
		t1.start();
		t1.join();
		t2.start();
		t2.join();
		
		
		System.out.println("value = "+obj.i);
	}
	
}
