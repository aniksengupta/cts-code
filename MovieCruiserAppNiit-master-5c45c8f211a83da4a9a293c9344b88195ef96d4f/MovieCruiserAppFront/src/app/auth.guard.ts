import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from './modules/authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  token : String
  constructor(private authService : AuthenticationService,private router : Router ){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      this.token= localStorage.getItem('token');
      //console.log(this.token);
      if(this.token === null){
          this.router.navigate(['/']);
          //console.log("from auth if");
          return false;
      }
      //console.log("from outside auth guard");
    return true;
  }

}
