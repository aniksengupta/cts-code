export class MovieBackend {
    id : number;
    movieId: number;
    userId : string;
    title: string;
    poster_path: string;
    comments: string;
    release_date: string;
    overview: string;
}