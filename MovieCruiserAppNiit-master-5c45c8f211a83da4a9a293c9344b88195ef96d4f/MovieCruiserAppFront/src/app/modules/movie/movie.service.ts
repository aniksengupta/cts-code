import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Movie } from './movie';
import { map } from 'rxjs/operators/map';
import { retry } from 'rxjs/operators';
import { MovieBackend } from './MovieBackend';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  movieBackendObj : MovieBackend = new MovieBackend();
  movieObj : MovieBackend = new MovieBackend();
  endpoint_search: string;
  tmdbEndpoint: string;
  imagePrefix: string;
  apiKey: string;
  constructor(private http: HttpClient) {
    this.apiKey = 'api_key=92298528a1629eeb89fdda95551e176e';
    this.tmdbEndpoint = 'https://api.themoviedb.org/3/movie';
    this.imagePrefix = 'https://image.tmdb.org/t/p/w500';
    this.endpoint_search = 'https://api.themoviedb.org/3';
  }

  getMovies(type: string, page: number = 1): Observable<Array<Movie>> {
    const endPoint = `${this.tmdbEndpoint}/${type}?${this.apiKey}&page=${page}`;
    return this.http.get(endPoint).pipe(
      retry(3),
      map(this.pickMovieResults),
      map(this.transformPosterPath.bind(this))
    );
  }

  transformPosterPath(movies): Array<Movie> {
    return movies.map(movie => {
      movie.poster_path = `${this.imagePrefix}${movie.poster_path}`;
      return movie;
    });
  }

  pickMovieResults(response) {
    return response['results'];
  }

  addMovieToWatchlist(movie){
    console.log("from service"+" "+movie.id);
    console.log(movie.vote_avg);
    this.movieObj= this.setBackendType(movie);
    return this.http.post('http://localhost:8066/movie/saveMovie',this.movieObj,{responseType : 'text'});
  }

  getWatchListedMovies(): Observable<Array<Movie>>{
    return this.http.get<Array<Movie>>("http://localhost:8066/movie/getAllMovie");
  }

  deleteFromWatchList(movie){
    console.log(movie.id);
    this.movieObj= this.setBackendType(movie);
    return this.http.delete('http://localhost:8066/movie/deleteMovie/'+movie.id,{responseType : 'text'});
  }
  updateWatchlist(movie){
    //console.log(movie.comments);
   // console.log("from update movie "+movie.id);
    // this.movieObj= this.setBackendType(movie);
    return this.http.put('http://localhost:8066/movie/updateMovie',movie,{responseType : 'text'});
  }
  
  searchMovie(searchKey: string,page: number = 1): Observable<Array<Movie>> {
    console.log(searchKey);
    if (searchKey.length > 0) {
      const searchEndpoint = `${this.endpoint_search}/search/movie?${this.apiKey}&page=${page}&include_adult=false&query=${searchKey}`;
      return this.http.get(searchEndpoint).pipe(
        retry(3),
        map(this.pickMovieResults),
        map(this.transformPosterPath.bind(this))
      );
    }
  }
  setBackendType(movie){
    this.movieBackendObj.movieId=movie.id;
    this.movieBackendObj.overview=movie.overview;
    this.movieBackendObj.poster_path=movie.poster_path;
    this.movieBackendObj.release_date=movie.release_date;
    this.movieBackendObj.title=movie.title;
    this.movieBackendObj.comments=movie.comments;
    return this.movieBackendObj;
  }
  }

