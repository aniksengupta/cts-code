import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '../../movie';
import { MovieService } from '../../movie.service';
import { ActivatedRoute } from '@angular/router';
import { ThumbnailComponent } from "../thumbnail/thumbnail.component";
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'movie-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {
  @Input()
  movies: Array<Movie>;
  @Input()
  useWatchListApi: boolean;


  constructor(private movieService: MovieService, private snackbar: MatSnackBar) {
  }

  ngOnInit() {

  }

  addToWatchList(movie) {

    console.log('from add to watch list method' + movie);
    this.movieService.addMovieToWatchlist(movie).subscribe((movie) => {
      console.log(movie); 
      this.snackbar.open('movie added to watchlist', '', {
        duration: 1000
      });
    });
  }

  deleteFromWatchList(movie) {
    console.log(movie.id + " from delete method");
    for (var i = 0; i < this.movies.length; i++) {
      if (this.movies[i].title === movie.title) {
        this.movies.splice(i, 1);
      }
    }
    this.movieService.deleteFromWatchList(movie).subscribe(res => {
      console.log(res + "hbh");

    })
  }

}
