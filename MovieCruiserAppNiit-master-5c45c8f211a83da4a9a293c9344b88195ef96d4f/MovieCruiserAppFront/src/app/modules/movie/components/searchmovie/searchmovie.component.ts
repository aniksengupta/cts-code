import { Component, OnInit } from '@angular/core';
import { Movie } from '../../movie';
import { MovieService } from '../../movie.service';
@Component({
  selector: 'app-searchmovie',
  templateUrl: './searchmovie.component.html',
  styleUrls: ['./searchmovie.component.css']
})
export class SearchmovieComponent implements OnInit {
  movies: Array<Movie>;
  constructor(private movieService : MovieService) { }

  ngOnInit() {
  }
  onEnter(searchKey) {
    console.log("from search"+searchKey);
    this.movieService.searchMovie(searchKey).subscribe(movies => {
      this.movies = movies;
    })
  }
}
