import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from '../../movie';
import { MatSnackBar} from '@angular/material/snack-bar'; 
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA}  from '@angular/material'; 
import { MoviedialogComponent } from '../moviedialog/moviedialog.component';


@Component({
  selector: 'movie-thumbnail',
  templateUrl: './thumbnail.component.html',
  styleUrls: ['./thumbnail.component.css']
})
export class ThumbnailComponent implements OnInit {
  
  @Input()
  movie: Movie

  @Input()
  useWatchListApi : boolean;
  
  @Output()
  addMovie=new EventEmitter();

  @Output()
  deleteMovie=new EventEmitter();


  constructor(private snackbar : MatSnackBar,private dialog : MatDialog) {
  }

  ngOnInit() {

  }
  addToWatchList(){
    this.addMovie.emit(this.movie);
    // console.log('from add to watch list method');
    // this.movieService.addMovieToWatchlist(this.movie).subscribe((movie)=>{
    //   console.log(movie);
    //   this.snackbar.open('movie added to watchlist','',{
    //     duration : 1000
    //   });
    // }); 
  }

  deleteFromWatchList(){
    this.deleteMovie.emit(this.movie);
  }

  updateFromWatchList(actionType){
    console.log('movie is getting updated');
    let dialogRef= this.dialog.open(MoviedialogComponent,{
      width : '400 px',
      data : {obj : this.movie,actionType : actionType}
    });
    console.log('open dialog');
    dialogRef.afterClosed().subscribe(result =>{
      console.log('closed');
    })
  }
}
