import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContainerComponent } from './container/container.component';
import {TmdbContainerComponent} from 'src/app/modules/movie/components/tmdb-container/tmdb-container.component';
import {WatchlistComponent} from 'src/app/modules/movie/components/watchlist/watchlist.component';
import { SelectorMatcher } from '@angular/compiler';
import {SearchmovieComponent} from 'src/app/modules/movie/components/searchmovie/searchmovie.component';
import { AuthGuard } from 'src/app/auth.guard';
const movieRoutes: Routes = [
{
    path: 'movies',
    children: [
        {
            path:'',
            redirectTo: '/movies/popular',
            pathMatch: 'full',
            canActivate: [AuthGuard]
        },
        {
            path:'popular',
            component: TmdbContainerComponent,
            canActivate: [AuthGuard],
            data: {
                movieType: 'popular'
            },
        },
        {
            path:'top_rated',
            component: TmdbContainerComponent,
            canActivate: [AuthGuard],
            data: {
                movieType: 'top_rated'
            },
        },
        {
            path :'watchlist',
            component: WatchlistComponent,
            canActivate: [AuthGuard]
        },
        {
            path : 'search',
            component: SearchmovieComponent,
            canActivate: [AuthGuard]
        }
    ]
}
]


@NgModule({
    imports: [
        RouterModule.forChild(movieRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class MovieRouterModule { }