import { Component, OnInit,Inject } from '@angular/core';
import {Movie} from 'src/app/modules/movie/movie';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MovieService} from 'src/app/modules/movie/movie.service';

@Component({
  selector: 'app-moviedialog',
  templateUrl: './moviedialog.component.html',
  styleUrls: ['./moviedialog.component.css']
})
export class MoviedialogComponent implements OnInit {
  movie: Movie;
  comments: string;
  actionType: string;
  constructor(private snacBar: MatSnackBar, private dialogRef: MatDialogRef<MoviedialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private movieService: MovieService) {

this.comments = data.obj.comments;
this.movie = data.obj;
this.actionType = data.actionType;
}
  ngOnInit() {
  }

  onNoClick() {
    this.dialogRef.close();
  }
  updateComments() {
    this.movie.comments = this.comments;
    console.log("from ts ", this.movie.comments)
    this.dialogRef.close();
    this.movieService.updateWatchlist(this.movie).subscribe(() => {
      this.snacBar.open("Movie updated to Watchlist successfully", "", {
        duration: 2000
      });
    })
    
  }
}
