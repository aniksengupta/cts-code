import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThumbnailComponent } from './components/thumbnail/thumbnail.component';
import { MovieService } from './movie.service';
import { HttpClientModule } from '@angular/common/http';
import { ContainerComponent } from './components/container/container.component';
import { RouterModule } from '@angular/router';
import { MovieRouterModule } from './components/movie-router.router';
import { MatCardModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import { TmdbContainerComponent } from './components/tmdb-container/tmdb-container.component';
import { WatchlistComponent } from './components/watchlist/watchlist.component';
import { MoviedialogComponent } from './components/moviedialog/moviedialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { SearchmovieComponent } from './components/searchmovie/searchmovie.component'
import { MovieBackend } from './MovieBackend';
import { TokenInterceptorService } from './token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
@NgModule({
  declarations: [ThumbnailComponent, ContainerComponent, TmdbContainerComponent, WatchlistComponent, MoviedialogComponent, SearchmovieComponent],
  entryComponents:[MoviedialogComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    RouterModule,
    MovieRouterModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    MatDialogModule,
    MatInputModule
  ],
  exports: [
    ThumbnailComponent,MoviedialogComponent,MovieRouterModule,WatchlistComponent,ContainerComponent,TmdbContainerComponent,SearchmovieComponent
  ],
  providers: [MovieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ]
})
export class MovieModule { }
