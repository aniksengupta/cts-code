
public class TenDigit {

	public String format(String input) {
		
		int len=input.length();
		String gap=" ";
		String output="";
		
		for(int i=0;i<10-len;i++) {
			output=output+gap;
		}
		
		output=output+input;
		return output;
		
	}
	
}
