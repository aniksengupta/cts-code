import {Login} from'./login.po';
import{browser,element,protractor} from 'protractor';

xdescribe("login page testing",()=>{

let loginpage : Login;
beforeEach(()=>{
    loginpage=new Login();
});

    it('successful admin login',()=>{

        loginpage.navigateto();
        loginpage.setEmail('sudip@gmail.com');
        loginpage.setPassword('Anik123@');
        loginpage.clickLoginButton();
        expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/adminHome');

    });

    it("for wrong credentials",()=>{
        loginpage.navigateto();
        loginpage.setEmail('sudiop@gmail.com');
        loginpage.setPassword('Anik123@');
        loginpage.clickLoginButton();
        var time=5000;
        browser.wait(protractor.ExpectedConditions.alertIsPresent(),time);
        var alertvar=browser.switchTo().alert();
        expect(alertvar.getText()).toEqual('wrong credentials');
        alertvar.accept();
        expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/Login');
    })

    it('successful user login',()=>{

        loginpage.navigateto();
        loginpage.setEmail('saswata@mail.com');
        loginpage.setPassword('Saswata123@');
        loginpage.clickLoginButton();
        expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/userHome');

    });

    it("for account block",()=>{
        loginpage.navigateto();
        loginpage.setEmail('a@a');
        loginpage.setPassword('Anik123@');
        loginpage.clickLoginButton();
        var time=5000;
        browser.wait(protractor.ExpectedConditions.alertIsPresent(),time);
        var alertvar=browser.switchTo().alert();
        expect(alertvar.getText()).toEqual('account is blocked. please contact admin');
        alertvar.accept();
        expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/Login');
    })


});
