import {browser,by,element} from 'protractor';
export class Login
{
    navigateto()
    {
        return browser.get('Login');
    }
    setEmail(email : string)
    {
        element(by.id('email')).sendKeys(email);
    }
    setPassword(password : string){
        element(by.id('password')).sendKeys(password);
    }
    clickLoginButton()
    {
        return element(by.id('login')).click();
    }
}