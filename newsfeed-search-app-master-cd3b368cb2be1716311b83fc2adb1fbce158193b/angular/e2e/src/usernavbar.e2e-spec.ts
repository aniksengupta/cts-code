import { usernavbar } from "./usernavbar.po";
import { browser,protractor } from 'protractor';
import { Login } from './login.po';


xdescribe("user navigation testing",()=>{

    let usernavbarpage : usernavbar;
    let loginpage: Login;
    beforeEach(()=>{
        usernavbarpage = new usernavbar();
    });
    
    it("logout testing",()=>{

       usernavbarpage.navigateto();
       usernavbarpage.clickLogoutButton();
       expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/"); 

    });

    it("navigate to History page",()=>{

        loginpage=new Login();
        loginpage.navigateto();
        loginpage.setEmail('saswata@mail.com');
        loginpage.setPassword('Saswata123@');
        loginpage.clickLoginButton();
        // expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/userHome");
        browser.pause(1000);
        usernavbarpage.navigateto();
        usernavbarpage.gotoHsitory();
         browser.pause(1000);
        expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/History");

    });

    it("deleting history testing",()=>{

        loginpage=new Login();
        loginpage.navigateto();
        loginpage.setEmail('saswata@mail.com');
        loginpage.setPassword('Saswata123@');
        usernavbarpage.navigateto();
        usernavbarpage.gotoHsitory();
        usernavbarpage.clickDeleteButton();
        browser.wait(protractor.ExpectedConditions.alertIsPresent(),3000);
        var alertvar=browser.switchTo().alert();
        expect(alertvar.getText()).toEqual('history deleted successfully');
        alertvar.accept();

     });
    });