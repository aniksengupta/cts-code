import { browser, element ,by} from 'protractor';

export class usernavbar{
    navigateto(){
    return browser.get('/usernavbar');
    }

    clickLogoutButton(){
        return element(by.id('Logout')).click();
    }

    gotoHsitory(){
        return element(by.id('history')).click();
    }

    clickDeleteButton(){
        return element(by.id('18')).click();
    }
}