import { browser,by,element } from 'protractor';


export class AdminNav{

    navigateToNavbar(){

        return browser.get('/adminnav');
    }

    clickLogoutButton(){
        return element(by.id('LogoutAdmin')).click();
    }
    clickAllUserLink(){
        return element(by.id('allUser')).click();
    }
    clickBlackListLink(){
        return element(by.id('blackUser')).click();
    }
    clickSearchUser(){
        return element(by.id('searchUser')).click();
    }
    setUserSearchString(searchstring : string){
        element(by.id('username')).sendKeys(searchstring);
    }
    clickSearchButtonInSearchPage()
    {
        return element(by.id('userSearch')).click();
    }

    clickDeleteOfUserActiveList(){
        return element(by.id('1')).click();
    }
}