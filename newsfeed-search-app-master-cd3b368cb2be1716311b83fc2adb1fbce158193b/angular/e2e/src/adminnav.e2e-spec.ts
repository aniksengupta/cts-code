import { AdminNav } from "./adminnav.po";
import { browser,element,by,protractor } from 'protractor';
import { Login } from './login.po';

describe("admin navigation testing",()=>{
let adminnavpage : AdminNav;
let loginpage : Login;
beforeEach(()=>{
    adminnavpage =new AdminNav();
    loginpage = new Login();
});

it("logout button testing whether going to app landing page",()=>{
        loginpage.navigateto();
        loginpage.setEmail('sudip@gmail.com');
        loginpage.setPassword('Anik123@');
        loginpage.clickLoginButton();
        expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/adminHome');
        adminnavpage.navigateToNavbar();
        adminnavpage.clickLogoutButton();
        expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/');
});
it("all user button checking whether going to that page or not",()=>{
    loginpage.navigateto();
    loginpage.setEmail('sudip@gmail.com');
    loginpage.setPassword('Anik123@');
    loginpage.clickLoginButton();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/adminHome');
    adminnavpage.navigateToNavbar();
    adminnavpage.clickAllUserLink();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/activeUsers');
});
it("blaclist link checking whether going to that page or not",()=>{
    loginpage.navigateto();
    loginpage.setEmail('sudip@gmail.com');
    loginpage.setPassword('Anik123@');
    loginpage.clickLoginButton();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/adminHome');
    adminnavpage.navigateToNavbar();
    adminnavpage.clickBlackListLink();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/blacklist');
});
it("search link checking whether going to that page or not",()=>{
    loginpage.navigateto();
    loginpage.setEmail('sudip@gmail.com');
    loginpage.setPassword('Anik123@');
    loginpage.clickLoginButton();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/adminHome');
    adminnavpage.navigateToNavbar();
    adminnavpage.clickSearchUser();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/searchuser');
});

it("after clicking search button card is coming or not",()=>{
    loginpage.navigateto();
    loginpage.setEmail('sudip@gmail.com');
    loginpage.setPassword('Anik123@');
    loginpage.clickLoginButton();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/adminHome');
    adminnavpage.navigateToNavbar();
    adminnavpage.clickSearchUser();
    adminnavpage.setUserSearchString('a');
    adminnavpage.clickSearchButtonInSearchPage();
    expect(element(by.className('mycard')).isPresent());
});
it("after clicking blacklist link table is coming or not",()=>{
    loginpage.navigateto();
    loginpage.setEmail('sudip@gmail.com');
    loginpage.setPassword('Anik123@');
    loginpage.clickLoginButton();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/adminHome');
    adminnavpage.navigateToNavbar();
    adminnavpage.clickBlackListLink();
    expect(element(by.id('blacklistTable')).isPresent());

});
it("after clicking all user link table is coming or not",()=>{
    loginpage.navigateto();
    loginpage.setEmail('sudip@gmail.com');
    loginpage.setPassword('Anik123@');
    loginpage.clickLoginButton();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/adminHome');
    adminnavpage.navigateToNavbar();
    adminnavpage.clickAllUserLink();
    expect(element(by.id('allUserTable')).isDisplayed());
});

it("clicking the first row delete button of Active user list",()=>{
    loginpage.navigateto();
    loginpage.setEmail('sudip@gmail.com');
    loginpage.setPassword('Anik123@');
    loginpage.clickLoginButton();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/adminHome');
    adminnavpage.navigateToNavbar();
    adminnavpage.clickAllUserLink();
    adminnavpage.clickDeleteOfUserActiveList();
    browser.wait(protractor.ExpectedConditions.alertIsPresent(),3000);
    var alertvar=browser.switchTo().alert();
    expect(alertvar.getText()).toEqual('user blocked successfully');

});
});