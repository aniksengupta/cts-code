import{Signup} from './signup.po';
import { browser, protractor } from 'protractor';

xdescribe('signup page testing',()=>{
    let signuppage : Signup;
    beforeEach(()=>{
        signuppage=new Signup();
    });
//successful registration will redirect to login page
it("registration successful",()=>{

    signuppage.navigateTo();
    signuppage.setName('sourabh ghosh');
    signuppage.setEmail('sourabh@mail.com');
    signuppage.setPassword('Sourabh123@');
    signuppage.clickSignupButton();
    var time=8000;
    browser.wait(protractor.ExpectedConditions.alertIsPresent(),time);
    var alertvar=browser.switchTo().alert();
    expect(alertvar.getText()).toEqual('registered successfully');
    alertvar.accept();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/Login');

});

it("email already exists test",()=>{

    signuppage.navigateTo();
    signuppage.setEmail('ankur@mail.com');
    signuppage.setName('Ankur bose');
    signuppage.setPassword('Ankur123@');
    signuppage.clickSignupButton();
    var time=3000;
    browser.wait(protractor.ExpectedConditions.alertIsPresent(),time);
    var alertvar=browser.switchTo().alert();
    expect(alertvar.getText()).toEqual('email already exists');
    alertvar.accept();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/');

});

});