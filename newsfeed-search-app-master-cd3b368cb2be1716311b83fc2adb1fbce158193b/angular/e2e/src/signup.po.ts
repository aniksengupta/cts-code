import { browser,element,by } from 'protractor';

export class Signup{
    navigateTo()
    {
        return browser.get('/');
    }
    setEmail(email : string)
    {
        element(by.id('email')).sendKeys(email);
    }
    setPassword(password : string)
    {
        element(by.id('password')).sendKeys(password);
    }
    setName(name : string){
        element(by.id('name')).sendKeys(name);
    }
    clickSignupButton()
    {
        return element(by.id('signup')).click();
    }
}