import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {SignupComponent} from 'src/app/Modules/signup/signup.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './Modules/login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NewAnalysthomeComponent } from 'src/app/Modules/new-analysthome/new-analysthome.component';
import { AdminhomeComponent } from 'src/app/Modules/adminhome/adminhome.component';
import { AdminnavbarComponent } from './Modules/adminnavbar/adminnavbar.component';
// import { ActiveuserComponent } from './modules/activeuser/activeuser.component';
//  import { BlacklistuserComponent } from './modules/blacklistuser/blacklistuser.component';
//  import { SearchuserComponent } from './modules/searchuser/searchuser.component';
// import {ActiveuserComponent} from 'src/app/Modules/activeuser/activeuser.component';
// import  {BlacklistuserComponent} from 'src/app/Modules/blacklistuser/blacklistuser.component';
// import{SearchuserComponent} from 'src/app/Modules/searchuser/searchuser.component';
import { SignupService } from './Services/signup.service';
import { LoginService } from './services/login.service';
import { AdminserviceService } from './services/adminservice.service';
import { HttpConfigInterceptor } from './Modules/HttpConfigInterceptor';
import { NewsnavbarComponent } from './Modules/newsnavbar/newsnavbar.component';
import { HistoryComponent } from 'src/app/Modules/history/history.component';
import { ActiveuserComponent } from 'src/app/Modules/Admin/activeuser/activeuser.component';
import { SearchuserComponent } from 'src/app/Modules/Admin/searchuser/searchuser.component';
import { BlacklistuserComponent } from 'src/app/Modules/Admin/blacklistuser/blacklistuser.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SignupComponent,
    LoginComponent,
    NewAnalysthomeComponent,
    AdminhomeComponent,
    AdminnavbarComponent,
    ActiveuserComponent,
    SearchuserComponent,
    BlacklistuserComponent,
    HistoryComponent,
    NewsnavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [SignupService,LoginService,AdminserviceService,
  {provide:HTTP_INTERCEPTORS,useClass:HttpConfigInterceptor,multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
