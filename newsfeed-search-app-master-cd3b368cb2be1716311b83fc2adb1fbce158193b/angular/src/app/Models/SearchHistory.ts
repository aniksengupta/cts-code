export class SearchHistory {
    searchHistoryId: Number;
    searchTopic: string;
    userEmail: string;
    searchTime: any
}