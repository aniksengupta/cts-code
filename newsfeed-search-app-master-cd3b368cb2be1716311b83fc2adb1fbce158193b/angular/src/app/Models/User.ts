export class User {
    userName: string;
    userEmail: string;
    userPassword: string;
    userStatus: string;
    role: string;
}