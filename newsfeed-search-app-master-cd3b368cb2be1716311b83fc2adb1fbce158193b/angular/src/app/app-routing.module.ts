import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SignupComponent} from 'src/app/Modules/signup/signup.component';
import { LoginComponent } from './Modules/login/login.component';
import { NewAnalysthomeComponent } from 'src/app/Modules/new-analysthome/new-analysthome.component';
import { AdminhomeComponent } from './Modules/adminhome/adminhome.component';
import { SearchuserComponent } from 'src/app/Modules/Admin/searchuser/searchuser.component';
import { HistoryComponent } from './Modules/history/history.component';
import {NewsnavbarComponent} from 'src/app/Modules/newsnavbar/newsnavbar.component';
import { AdminnavbarComponent } from './Modules/adminnavbar/adminnavbar.component';
import { ActiveuserComponent } from './Modules/Admin/activeuser/activeuser.component';
import { BlacklistuserComponent } from './Modules/Admin/blacklistuser/blacklistuser.component';


const routes: Routes = [{
  path:'',
  component: SignupComponent
},
{
  path : 'Login',
  component:LoginComponent
},
{
  path : 'userHome',
  component: NewAnalysthomeComponent
},
{
  path :'adminHome',
  component:AdminhomeComponent
},
{
  path:'activeUsers',
  component:ActiveuserComponent
},
{
  path :'blacklist',
  component : BlacklistuserComponent
},
{
  path: 'searchuser',
  component: SearchuserComponent
},
{
  path : 'History',
  component : HistoryComponent
},
  {
     path : 'usernavbar',
     component: NewsnavbarComponent
  },
  {
    path : 'adminnav',
    component : AdminnavbarComponent
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
