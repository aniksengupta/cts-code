import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SearchHistory } from '../Models/SearchHistory';
@Injectable({
  providedIn: 'root'
})
export class NewsanalystService {

  userName: string;
  api_key = "20a484ff97bb4c42b91617727fb0730a";

  constructor(private http: HttpClient) {
    console.log("Inside NewsAnalystService Constructor.")
  }
  initSources() {
    return this.http.get('https://newsapi.org/v2/sources?language=en&apiKey=' + this.api_key);
  }
  initArticles() {
    return this.http.get('https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=' + this.api_key);
  }
  getArticlesByID(obj: SearchHistory) {
    console.log(obj.searchTopic+" "+"method");
    return this.http.get('https://newsapi.org/v2/everything?q=' + obj.searchTopic + '&apiKey=' + this.api_key);
  }

  saveSearch(obj: SearchHistory) {
    obj.userEmail = window.sessionStorage.getItem('UserName');
    console.log(obj.userEmail);
    this.http.post('http://localhost:8075/NewsFeed/common/saveSearch', obj, { responseType: 'text' }).subscribe(data => {
      console.log(data);
    })
  }

  loadHistory() {
    this.userName = window.sessionStorage.getItem('UserName');
    console.log("from service load History" + " " + this.userName);
    return this.http.get<Array<SearchHistory>>('http://localhost:8075/NewsFeed/common/getHistory/' + this.userName);
  }

  deleteSearchHistory(searchhistory: SearchHistory) {
    console.log(searchhistory.searchTopic + " " + "from service");
    return this.http.delete('http://localhost:8075/NewsFeed/common/deleteHistory/' + searchhistory.searchHistoryId, { responseType: 'text' });
  }
}
