import { TestBed } from '@angular/core/testing';

import { NewsanalystService } from './newsanalyst.service';

describe('NewsanalystService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewsanalystService = TestBed.get(NewsanalystService);
    expect(service).toBeTruthy();
  });
});
