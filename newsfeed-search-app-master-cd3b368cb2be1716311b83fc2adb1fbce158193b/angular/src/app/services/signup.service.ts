import { Injectable } from '@angular/core';
import { User } from 'src/app/Models/User';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { SignupStatus } from 'src/app/Models/SignupStatus';

@Injectable({
  providedIn: 'root'
})
export class SignupService {
  signupStatus: SignupStatus = new SignupStatus;
  constructor(private http: HttpClient, private router: Router) { }

  signup(user: User) {
    user.userStatus = "true";
    user.role = "ROLE_USER";
    console.log(user.userEmail + " " + user.role + " " + user.userStatus);
    console.log("from service");
    this.http.post<SignupStatus>("http://localhost:8075/NewsFeed/user/registerUser", user).subscribe(responsedata => {
      this.signupStatus = responsedata;
      console.log(this.signupStatus);
      if (this.signupStatus.signupStatus) {
        alert("registered successfully");
        this.router.navigate(['Login']);
      }
      else {
        alert('email already exists');
      }

    })
  }

}
