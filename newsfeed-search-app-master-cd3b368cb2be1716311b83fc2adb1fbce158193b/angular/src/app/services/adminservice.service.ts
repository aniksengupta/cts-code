import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../Models/User';
import { text } from '@angular/core/src/render3';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';

@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {
  email: string;
  constructor(private http: HttpClient) { }

  getAllActivateUser() {
    console.log("from service");
    return this.http.get<User>("http://localhost:8075/NewsFeed/common/getActivateUser")

  }

  showAllDeactivatedUser(): Observable<User> {
    console.log("from blacklist service");
    return this.http.get<User>("http://localhost:8075/NewsFeed/common/getDeactivateUser");
  }

  deactivateUser(user: User) {
    this.email = user.userEmail;
    console.log("from deactivate method" + " " + this.email);
    return this.http.put("http://localhost:8075/NewsFeed/common/blackList/" + this.email, '', { responseType: 'text' });
  }
  //yet to implement 
  searchUser(user: User) {
    console.log("from search service");
    return this.http.get<User>('http://localhost:8075/NewsFeed/common/searchUser/' + user.userName);
  }

  activateUser(user: User) {
    this.email = user.userEmail;
    console.log("from activate method" + " " + this.email);
    return this.http.put("http://localhost:8075/NewsFeed/common/activate/" + this.email, '', { responseType: 'text' });
  }
}