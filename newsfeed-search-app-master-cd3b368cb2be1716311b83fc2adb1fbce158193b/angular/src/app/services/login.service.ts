import { Injectable } from '@angular/core';
import { User } from '../Models/User';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private router: Router) { }
  login(user: User) {
    console.log("from service");
    console.log(user.userEmail + " " + user.userPassword);
    //for admin login
    if (user.userEmail == "sudip@gmail.com") {
      console.log("inside if");
      this.http.post("http://localhost:8075/NewsFeed/user/login", user, { responseType: 'text' }).subscribe(responseData => {
        console.log(responseData);
        window.sessionStorage.setItem('Token', responseData);
        window.sessionStorage.setItem('UserName', user.userEmail);
        this.router.navigate(['adminHome']);
      },
        error => {
          alert("wrong credentials");
        });
    }
    //for user login
    else {

      console.log("from else");
      this.http.post("http://localhost:8075/NewsFeed/user/login", user, { responseType: 'text' }).subscribe(responseData => {
        console.log(responseData);
        if (responseData == "account is blocked") {
          alert("account is blocked. please contact admin");
          return;
        }
        window.sessionStorage.setItem('Token', responseData);
        window.sessionStorage.setItem('UserName', user.userEmail);
        this.router.navigate(['userHome']);
      },
        error => {
          alert("wrong credentials");
        });

    }
  }
}
