import { Component, OnInit } from '@angular/core';
import { AdminserviceService } from 'src/app/services/adminservice.service';
import { User } from 'src/app/Models/User';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
@Component({
  selector: 'app-blacklistuser',
  templateUrl: './blacklistuser.component.html',
  styleUrls: ['./blacklistuser.component.css']
})
export class BlacklistuserComponent implements OnInit {
  list: any;
  noBlackListedVisibility: boolean = false;
  blackListUser: boolean = true;
  constructor(private adminservice: AdminserviceService) { }

  ngOnInit() {
    this.showAllDeactivatedUser();
  }

  showAllDeactivatedUser() {
    console.log("from blacklist controller");
    this.adminservice.showAllDeactivatedUser().subscribe(data => {
      this.list = data;
      if (this.list.length > 0) {
        this.blackListUser = true;
        console.log("from if");
      } else {
        this.noBlackListedVisibility = true;
        this.blackListUser = false;
      }
    });
  }
  activateUser(user: User) {
    console.log(user.userEmail + " " + user.userName);
    this.adminservice.activateUser(user).subscribe(responseData => {
      alert('user activated successfully');
      this.showAllDeactivatedUser();
    });
  }
}
