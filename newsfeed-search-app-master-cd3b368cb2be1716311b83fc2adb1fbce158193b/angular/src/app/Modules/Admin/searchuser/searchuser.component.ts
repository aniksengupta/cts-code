import { Component, OnInit } from '@angular/core';
import { AdminserviceService } from 'src/app/services/adminservice.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/Models/User';

@Component({
  selector: 'app-searchuser',
  templateUrl: './searchuser.component.html',
  styleUrls: ['./searchuser.component.css']
})
export class SearchuserComponent implements OnInit {
  list: any;
  searchBlankVisibility: boolean = false;
  visible = false;
  searchform: FormGroup;
  constructor(private formBuilder: FormBuilder, private router: Router, private adminService: AdminserviceService) { }

  ngOnInit() {
    //this.submitted=false;
    this.searchform = this.formBuilder.group({
      userName: ['', [Validators.required]],
    });
  }
  //yet to implements
  searchUser(user: User) {
    this.visible = true;
    console.log("from search" + " " + user.userName + " " + this.visible);
    this.adminService.searchUser(user).subscribe(data => {
      this.list = data;
      console.log(data);
      if (this.list.length <= 0) {
        console.log("from if");
        this.visible = false;
        this.searchBlankVisibility = true;
      } else {
        this.visible = true;
        this.searchBlankVisibility = false;
      }
    })
  }

  blackListUser(user: User) {
    console.log(user.userEmail + " " + user.userName);
    this.adminService.deactivateUser(user).subscribe(responseData => {
      alert("User deactivated successfully");
      this.adminService.getAllActivateUser();
    })
  }
}
