import { Component, OnInit } from '@angular/core';
import {AdminserviceService} from 'src/app/services/adminservice.service';
import {User} from 'src/app/Models/User';
import {Router} from '@angular/router';
@Component({
  selector: 'app-activeuser',
  templateUrl: './activeuser.component.html',
  styleUrls: ['./activeuser.component.css']
})
export class ActiveuserComponent implements OnInit {
  list:any;
  i=1;
  constructor(private adminService : AdminserviceService,private router: Router) { }

  ngOnInit() {
    this.getAllActivateUser();
  }

  getAllActivateUser()
  {
    console.log('getting all activate users for admin');
    this.adminService.getAllActivateUser().subscribe(data=>{
      this.list=data;
    });
  }

  deactivateUser(user : User){
    console.log("from deactivate method"+" "+user.userEmail);
    this.adminService.deactivateUser(user).subscribe(responsedata=>{
      console.log(responsedata);
      alert('user blocked successfully');
      this.getAllActivateUser();
    });
  }
}
