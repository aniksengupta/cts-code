import { Component, OnInit } from '@angular/core';
import { NewsanalystService } from 'src/app/services/newsanalyst.service';
import { SearchHistory } from 'src/app/Models/SearchHistory';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-analysthome',
  templateUrl: './new-analysthome.component.html',
  styleUrls: ['./new-analysthome.component.css']
})
export class NewAnalysthomeComponent implements OnInit {
  visibilityOfNews: boolean = true;
  searchResult: boolean = false;
  blankSearchVisibility: boolean = false;
  mArticles: Array<any>;
  mSources: Array<any>;
  searchForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private newsService: NewsanalystService, private router: Router) { }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      searchTopic: ['', Validators.required],

    });
    //load articles
    this.newsService.initArticles().subscribe(data => this.mArticles = data['articles']);
    //load news sources
    this.newsService.initSources().subscribe(data => this.mSources = data['sources']);
  }

  searchArticles(obj: SearchHistory) {
    this.mArticles = []
    if (this.searchForm.invalid) {
      this.searchResult = false;
      this.blankSearchVisibility = true;
      this.router.navigate(['/userHome']);
      this.newsService.initArticles().subscribe(data => this.mArticles = data['articles']);
      //load news sources
      //this.newsService.initSources().subscribe(data => this.mSources = data['sources']);
     
    }
    else {
      this.blankSearchVisibility = false;
      console.log("from");
      this.newsService.getArticlesByID(obj).subscribe(data => {
        this.mArticles = data['articles'];
        console.log(this.mArticles);
        if (this.mArticles.length > 0) {

          this.visibilityOfNews = true;
          this.searchResult = false;
        }
        else {

          this.visibilityOfNews = false;
          this.searchResult = true;
        }
      });
      this.newsService.saveSearch(obj);
    }
  }
}
