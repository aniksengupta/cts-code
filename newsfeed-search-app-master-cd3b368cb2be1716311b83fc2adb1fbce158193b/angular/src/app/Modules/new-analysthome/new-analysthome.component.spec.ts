import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAnalysthomeComponent } from './new-analysthome.component';

describe('NewAnalysthomeComponent', () => {
  let component: NewAnalysthomeComponent;
  let fixture: ComponentFixture<NewAnalysthomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewAnalysthomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAnalysthomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
