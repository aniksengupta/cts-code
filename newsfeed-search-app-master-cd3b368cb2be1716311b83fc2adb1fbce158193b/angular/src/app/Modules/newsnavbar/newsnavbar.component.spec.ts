import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsnavbarComponent } from './newsnavbar.component';

describe('NewsnavbarComponent', () => {
  let component: NewsnavbarComponent;
  let fixture: ComponentFixture<NewsnavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsnavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsnavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
