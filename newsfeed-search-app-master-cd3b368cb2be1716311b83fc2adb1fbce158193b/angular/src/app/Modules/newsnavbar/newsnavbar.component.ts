import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-newsnavbar',
  templateUrl: './newsnavbar.component.html',
  styleUrls: ['./newsnavbar.component.css']
})
export class NewsnavbarComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit() {
  }

  logoutUser()
  {
    window.sessionStorage.clear();
    this.router.navigate(['']);
    
  }

}
