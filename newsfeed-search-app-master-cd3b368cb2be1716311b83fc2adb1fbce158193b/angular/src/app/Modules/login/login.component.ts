import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/Models/User';
import {LoginService} from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted : boolean;
  

  constructor(private formBuilder: FormBuilder, private router: Router,private loginService : LoginService ) { }

  ngOnInit() {
    this.submitted=false;
    this.loginForm = this.formBuilder.group({
      userEmail: ['', [Validators.required]],
      userPassword: ['', [Validators.required]]
    });
  }

  get form() { return this.loginForm.controls; }

  onSubmit(user : User) {
    console.log("from ts");
    this.submitted=true;
    if (this.loginForm.invalid) {
      return;
    }
   this.loginService.login(user);
  }

}
