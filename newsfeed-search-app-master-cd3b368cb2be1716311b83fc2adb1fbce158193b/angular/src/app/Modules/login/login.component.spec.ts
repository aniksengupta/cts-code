import { LoginComponent } from "./login.component";
import { NavbarComponent } from 'src/app/navbar/navbar.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
fdescribe("loginComponent",()=>{
    let login : LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    beforeEach(()=>{
        TestBed.configureTestingModule({
            declarations :[NavbarComponent,LoginComponent],
            imports :[HttpClientModule,ReactiveFormsModule,FormsModule,RouterTestingModule],
        }).compileComponents();
        fixture=TestBed.createComponent(LoginComponent);
        login=fixture.componentInstance;
        fixture.detectChanges();
    });

    it("is login component defined",()=>{
            expect(login).toBeDefined();
    });
    it("is login form valid on giving correct userEmail and password",()=>{
        let userEmail= login.loginForm.controls['userEmail'];
        userEmail.setValue('a@a.com');
        let userPassword=login.loginForm.controls['userPassword'];
        userPassword.setValue('Anik123@');
        expect(login.loginForm.valid).toBeTruthy();
    });

    it("is login form invalid on giving only userEmail and no password",()=>{
        let userEmail= login.loginForm.controls['userEmail'];
        userEmail.setValue('a@a.com');
        let userPassword=login.loginForm.controls['userPassword'];
        userPassword.setValue('');
        expect(login.loginForm.invalid).toBeTruthy();
    });

    it("is login form invalid on giving only userPassword and no usermail",()=>{
        let userEmail= login.loginForm.controls['userEmail'];
        userEmail.setValue('');
        let userPassword=login.loginForm.controls['userPassword'];
        userPassword.setValue('Anik123@');
        expect(login.loginForm.invalid).toBeTruthy();
    });

    it("is login form invalid without giving anything",()=>{
        let userEmail= login.loginForm.controls['userEmail'];
        userEmail.setValue('');
        let userPassword=login.loginForm.controls['userPassword'];
        userPassword.setValue('');
        expect(login.loginForm.invalid).toBeTruthy();
    });

    //message part
    it("is login form invalid when userEmail is not given",()=>{
        let userEmail=login.loginForm.controls['userEmail'];
        userEmail.setValue('');
        let userPassword=login.loginForm.controls['userPassword'];
        userPassword.setValue('Anik123@');
        fixture.debugElement.query(By.css('#login')).nativeElement.click();
        fixture.detectChanges();
        let errorMessage=fixture.debugElement.nativeElement.querySelector('#emailEmpty');
        expect(errorMessage.innerHTML).toContain('Email is required');
    });

    it("is login form invalid when userPassword is not given",()=>{
        let userEmail=login.loginForm.controls['userEmail'];
        userEmail.setValue('aaa@mail.com');
        let userPassword=login.loginForm.controls['userPassword'];
        userPassword.setValue('');
        fixture.debugElement.query(By.css('#login')).nativeElement.click();
        fixture.detectChanges();
        let errorMessage=fixture.debugElement.nativeElement.querySelector('#passwordrequired');
        expect(errorMessage.innerHTML).toContain('Password is required');
    });

})