import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SignupComponent } from './signup.component';
import { FormsModule, ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms';
import { SignupService } from 'src/app/Services/signup.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { Signup } from 'e2e/src/signup.po';
import { NavbarComponent } from 'src/app/navbar/navbar.component';
import { NgModule } from '@angular/core';
import { By } from '@angular/platform-browser';
import { SignupStatus } from 'src/app/Models/SignupStatus';

fdescribe('SignupComponent', () => {
  let signup: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;
  let signupService : SignupService;
  let signupStatus : SignupStatus;
  
  beforeEach(()=>{
    TestBed.configureTestingModule(
      {
      declarations: [SignupComponent,NavbarComponent],
      imports:[FormsModule,HttpClientModule,ReactiveFormsModule,RouterTestingModule],

    }).compileComponents();

    fixture=TestBed.createComponent(SignupComponent);
    signup=fixture.componentInstance;
    signupService=TestBed.get(SignupService);
    fixture.detectChanges();
    signupStatus=new SignupStatus();
  });

  it('is signup component defined',()=>{
      expect(signup).toBeDefined();
  });
  
  it('is signup valid',()=>{
      let userName=signup.registerForm.controls["userName"];
      userName.setValue('Anik Sengupta');
      let userEmail=signup.registerForm.controls['userEmail'];
      userEmail.setValue('a@a');//matching the format
      let userPassword=signup.registerForm.controls["userPassword"];
      userPassword.setValue('Anik123@');
      expect(signup.registerForm.valid).toBeTruthy();
  });

  it('is signup invalid for not matching password format',()=>{
    let userName=signup.registerForm.controls["userName"];
    userName.setValue('Anik Sengupta');
    let userEmail=signup.registerForm.controls['userEmail'];
    userEmail.setValue('a@a');//matching the format
    let userPassword=signup.registerForm.controls["userPassword"];
    userPassword.setValue('Anik13');
    expect(signup.registerForm.valid).toBeFalsy();
});


it('is signup invalid for not matching email format',()=>{
  let userName=signup.registerForm.controls["userName"];
  userName.setValue('Anik Sengupta');
  let userEmail=signup.registerForm.controls['userEmail'];
  userEmail.setValue('anik.com');//not matching the format
  let userPassword=signup.registerForm.controls["userPassword"];
  userPassword.setValue('Anik123@');
  expect(signup.registerForm.valid).toBeFalsy();
});


it('is signup invalid for giving blank in name field',()=>{
  let userName=signup.registerForm.controls["userName"];
  userName.setValue(' ');
  let userEmail=signup.registerForm.controls['userEmail'];
  userEmail.setValue('anik@com');//matching the format
  let userPassword=signup.registerForm.controls["userPassword"];
  userPassword.setValue('Anik123@');
  expect(signup.registerForm.valid).toBeFalsy();
});

it('is signup invalid for giving blank in email field',()=>{
  let userName=signup.registerForm.controls["userName"];
  userName.setValue('Anik sengupta');
  let userEmail=signup.registerForm.controls['userEmail'];
  userEmail.setValue('');//giving null value
  let userPassword=signup.registerForm.controls["userPassword"];
  userPassword.setValue('Anik123@');
  expect(signup.registerForm.valid).toBeFalsy();
});

//message testing starts

it('is signup invalid for giving blank in email field',()=>{
  let userName=signup.registerForm.controls["userName"];
  userName.setValue('Anik sengupta');
  let userEmail=signup.registerForm.controls['userEmail'];
  userEmail.setValue('');//giving null email value
  let userPassword=signup.registerForm.controls["userPassword"];
  userPassword.setValue('Anik123@');
 fixture.debugElement.query(By.css('#signup')).nativeElement.click();
 fixture.detectChanges();
 let error= fixture.debugElement.nativeElement.querySelector('#error1');
 expect(error.innerHTML).toContain('Email is required');
});

it("is signup invalid on giving email which does not match the pattern",()=>{
    let userName=signup.registerForm.controls['userName'];
    userName.setValue('Anik sengupta');
    let userEmail=signup.registerForm.controls['userEmail'];
    userEmail.setValue('aaa.com');// email does not match
    let userPassword=signup.registerForm.controls['userPassword'];
    userPassword.setValue('Anik123@');
    fixture.debugElement.query(By.css("#signup")).nativeElement.click();
    fixture.detectChanges();
    let error=fixture.debugElement.nativeElement.querySelector('#error2');
    expect(error.innerHTML).toContain('Email must be a valid email address');
});

it("is signup invalid on giving name as blank",()=>{
  let userName=signup.registerForm.controls['userName'];
  userName.setValue('');// blank name
  let userEmail=signup.registerForm.controls['userEmail'];
  userEmail.setValue('aaa@mail.com');
  let userPassword=signup.registerForm.controls['userPassword'];
  userPassword.setValue('Anik123@');
  fixture.debugElement.query(By.css("#signup")).nativeElement.click();
  fixture.detectChanges();
  let error=fixture.debugElement.nativeElement.querySelector('#nameerror1');
  expect(error.innerHTML).toContain('Name is required');
});

it("is signup invalid on giving numbers in name field",()=>{
  let userName=signup.registerForm.controls['userName'];
  userName.setValue('Anik12');// name with numbers
  let userEmail=signup.registerForm.controls['userEmail'];
  userEmail.setValue('aaa@mail.com');
  let userPassword=signup.registerForm.controls['userPassword'];
  userPassword.setValue('Anik123@');
  fixture.debugElement.query(By.css("#signup")).nativeElement.click();
  fixture.detectChanges();
  let error=fixture.debugElement.nativeElement.querySelector('#nameerror2');
  expect(error.innerHTML).toContain('Name must have first name and last name,Name can only be alphabets');
});

it("is signup invalid when password is null",()=>{
  let userName=signup.registerForm.controls['userName'];
  userName.setValue('Anik Sen');
  let userEmail=signup.registerForm.controls['userEmail'];
  userEmail.setValue('aaa@mail.com');
  let userPassword=signup.registerForm.controls['userPassword'];
  userPassword.setValue('');//blank password
  fixture.debugElement.query(By.css("#signup")).nativeElement.click();
  fixture.detectChanges();
  let error=fixture.debugElement.nativeElement.querySelector('#passworderror1');
  expect(error.innerHTML).toContain('Password is required');
});

it("is signup invalid when password is not matching the pattern",()=>{
  let userName=signup.registerForm.controls['userName'];
  userName.setValue('Anik Sen');
  let userEmail=signup.registerForm.controls['userEmail'];
  userEmail.setValue('aaa@mail.com');
  let userPassword=signup.registerForm.controls['userPassword'];
  userPassword.setValue('Anik1234');// not matching the pattern
  fixture.debugElement.query(By.css("#signup")).nativeElement.click();
  fixture.detectChanges();
  let error=fixture.debugElement.nativeElement.querySelector('#passworderror2');
  expect(error.innerHTML).toContain('Password must contain at least eight characters, one capital letter, one small letter and at least one number');
});

it("is signup invalid when not giving last name",()=>{
  let userName=signup.registerForm.controls['userName'];
  userName.setValue('Aniken');//not giving last name
  let userEmail=signup.registerForm.controls['userEmail'];
  userEmail.setValue('aaa@mail.com');
  let userPassword=signup.registerForm.controls['userPassword'];
  userPassword.setValue('Anik123@');// not matching the pattern
  fixture.debugElement.query(By.css("#signup")).nativeElement.click();
  fixture.detectChanges();
  let error=fixture.debugElement.nativeElement.querySelector('#nameerror2');
  expect(error.innerHTML).toContain('Name must have first name and last name,Name can only be alphabets');
});

//mocking signup service
it("check signup service with valid data",()=>{
let spy=spyOn(signupService,'signup').and.callFake(()=>{
  signup.message="registered successfully";

});
let userName=signup.registerForm.controls['userName'];
  userName.setValue('Anik sen');
  let userEmail=signup.registerForm.controls['userEmail'];
  userEmail.setValue('aaa@mail.com');
  let userPassword=signup.registerForm.controls['userPassword'];
  userPassword.setValue('Anik123@');
  fixture.debugElement.query(By.css("#signup")).nativeElement.click();
  expect(spy).toHaveBeenCalledTimes(1);
  expect(signup.message).toContain('registered successfully');

});
it("check signup service with signupStatus object",()=>{
  let spy=spyOn(signupService,'signup').and.callFake(()=>{
    signupStatus.errorMessage="email already exists";
    signupStatus.signupStatus=false;

  });
  let userName=signup.registerForm.controls['userName'];
  userName.setValue('Anik sen');
  let userEmail=signup.registerForm.controls['userEmail'];
  userEmail.setValue('aaa@mail.com');
  let userPassword=signup.registerForm.controls['userPassword'];
  userPassword.setValue('Anik123@');
  fixture.debugElement.query(By.css("#signup")).nativeElement.click();
  expect(spy).toHaveBeenCalledTimes(1);
  expect(signupStatus).toEqual(signupStatus);
});

})