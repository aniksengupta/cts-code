import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import {User} from 'src/app/Models/User';
import { LoginComponent } from '../login/login.component';
import { SignupService } from 'src/app/Services/signup.service';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  registerForm: FormGroup;
  submitted: boolean;
  status: String='';
  message : string;
  constructor(private formBuilder: FormBuilder,
    private router: Router,private signupService: SignupService
    ) { }

  ngOnInit() {
    this.submitted = false;
    this.registerForm = this.formBuilder.group({
      userName: ['', [Validators.required, Validators.minLength(4), Validators.pattern('^[a-zA-Z]+[ ]+[a-zA-Z]+$')]],
      userEmail: ['', [Validators.required, Validators.email]],
      userPassword: ['', [Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!,@,#,$]).{8,}$')]]
    });
  }

   get f() { 
  //   console.log("inside get f()");
    return this.registerForm.controls; 
           }

  onSubmit(user: User) {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    console.log("from controller");
    this.signupService.signup(user);
  }
    login(){
      this.router.navigate(['Login']);
    }
    
   
    // this.signupService.signup(user).subscribe(data=>{
    //   this.status=data.toString();
    //   console.log(this.status);
    //   if (this.status.match('Registered Successfully!'))
    //     Swal.fire({
    //       title: "Registration Successfull!",
    //       text: "You will now be redirected to the login page..!",
    //       type: "success"
    //     }).then(() => {
    //       this.router.navigate(['Login']);
    //     });
    //   else 
    //     Swal.fire({
    //       title: "Registration Unsuccessfull!",
    //       text: "Please try again..!",
    //       type: "error"
    //     });
    // });


}
