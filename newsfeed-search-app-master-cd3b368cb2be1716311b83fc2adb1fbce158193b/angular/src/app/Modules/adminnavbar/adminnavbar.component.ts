import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-adminnavbar',
  templateUrl: './adminnavbar.component.html',
  styleUrls: ['./adminnavbar.component.css']
})
export class AdminnavbarComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit() {
  }
  logoutAdmin()
  {
    console.log("from logout method");
    window.sessionStorage.clear();
    this.router.navigate(['']);

  }

}
