import { Component, OnInit } from '@angular/core';
import { NewsanalystService } from 'src/app/services/newsanalyst.service';
import { SearchHistory } from 'src/app/Models/SearchHistory';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  list: Array<SearchHistory>;

  searchHistoryVisibility: boolean = true;
  noSearchHistoryVisibility: boolean = false;
  constructor(private newsservice: NewsanalystService) { 
    this.list=new Array<SearchHistory>();
  }

  ngOnInit() {
    this.loadHistory();
  }
  //load  search history 
  loadHistory() {
    console.log("from history");
    this.newsservice.loadHistory().subscribe(data => {
      this.list = data;
    });
  }

  deleteSearchHistory(searchhistory: SearchHistory) {
    console.log(searchhistory.searchTopic + " " + "from ts");
    this.newsservice.deleteSearchHistory(searchhistory).subscribe(data => {
      console.log(data);
      alert("history deleted successfully");
      this.loadHistory();
    })
  }
}
