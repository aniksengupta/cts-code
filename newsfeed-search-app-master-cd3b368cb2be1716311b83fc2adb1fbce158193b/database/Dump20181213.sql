CREATE DATABASE  IF NOT EXISTS `news` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `news`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: news
-- ------------------------------------------------------
-- Server version	5.1.45-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_email` varchar(255) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `user_name` varchar(25) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_status` bit(1) NOT NULL,
  PRIMARY KEY (`user_email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('dada@gmail.com','ROLE_USER','Vd das','$2a$10$pzPExbMfuHmndEGrWcv5DuC/XhF4JbwIXTlFTrxvnpsDVZK0cdLsS','\0'),('bhai@gmail.com','ROLE_USER','sd dss','$2a$10$UgBf4a92diASmjmwotPaMuNIq5k3ULwGOtcu//VqrJbCsnRwb.VnW','\0'),('a@a','ROLE_USER','Asdds As','$2a$10$GT4ggieV8rxIwvKGiHnr8.OaRWy/VbfkkePmDfxpiCm9dXJQd/LaC',''),('sudip@gmail.com','ROLE_ADMIN','Sudip Pain','$2a$10$2zIMX1glDEPGQJv5l3g4PukxEi55a3KAAo7NHVdxxkCArqXuCpace',''),('sayan@mail.com','ROLE_USER','sayan das','$2a$10$B7bbYi8jqPwtzauhfaqND.P1Gi7LgIR5fxDcLbrRLTn09VMbEVme.','\0'),('saswata@mail.com','ROLE_USER','Sas wata','$2a$10$BHfyiqyFUF9kBSMhLcYAvu1AYIqD1cAZPpJ/wvyg8wamCH9v348WW',''),('sourabh@mail.com','ROLE_USER','sourabh ghosh','$2a$10$zW0bIqC.Wc3i/2GdDFs8z.zrAdDfd4h3rOTmp1B8VFFFqlpT6ve/a',''),('ankur@mail.com','ROLE_USER','Ankur Das','$2a$10$LYWxDieauY9aMwMDJUvtXOCIwb1DCY6bnL.kwYZXE6PN7gNwk.LlC',''),('sdb@mail.com','ROLE_USER','smaranika debroy','$2a$10$hGIWHkSOCmHN.7ac9AbgcOQnCToiLskubDuIaH/Wnuje9exL7C/6K','');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_history`
--

DROP TABLE IF EXISTS `search_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_history` (
  `search_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `search_time` datetime DEFAULT NULL,
  `search_topic` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`search_history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_history`
--

LOCK TABLES `search_history` WRITE;
/*!40000 ALTER TABLE `search_history` DISABLE KEYS */;
INSERT INTO `search_history` VALUES (30,'2019-02-22 10:08:50','america','saswata@mail.com'),(29,'2019-02-21 17:51:15','america','saswata@mail.com'),(28,'2019-02-21 17:36:53','america','saswata@mail.com'),(23,'2019-02-20 12:49:26','roadies','saswata@mail.com'),(24,'2019-02-20 12:50:47','big boss','saswata@mail.com'),(31,'2019-02-22 10:13:41','america','saswata@mail.com'),(32,'2019-02-22 10:16:14','america','saswata@mail.com'),(33,'2019-02-22 10:34:37','america','saswata@mail.com'),(34,'2019-02-22 10:47:17','america','saswata@mail.com'),(35,'2019-02-22 10:50:20','america','saswata@mail.com'),(36,'2019-02-25 17:12:19','america','saswata@mail.com'),(37,'2019-02-25 17:13:39','america','saswata@mail.com'),(38,'2019-02-25 17:45:13','america','saswata@mail.com'),(39,'2019-02-25 17:48:48','america','saswata@mail.com'),(40,'2019-02-25 17:51:09','america','saswata@mail.com'),(41,'2019-02-25 18:03:08','deepika','sdb@mail.com');
/*!40000 ALTER TABLE `search_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-26 15:33:29
