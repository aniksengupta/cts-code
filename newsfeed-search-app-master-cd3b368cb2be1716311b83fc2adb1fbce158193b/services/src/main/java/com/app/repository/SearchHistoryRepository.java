package com.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.app.entity.SearchHistory;

public interface SearchHistoryRepository extends CrudRepository<SearchHistory, Integer> {
	// @Query("select s from SearchHistory s where s.userEmail=:userEmail")
	// public List<SearchHistory> findSearchHistoryByUserEmail(String
	// userEmail);
	public List<SearchHistory> findSearchHistoryByUserEmail(String userEmail);

	// public boolean deleteBySearchHistoryId(int searchHistoryId);
}
