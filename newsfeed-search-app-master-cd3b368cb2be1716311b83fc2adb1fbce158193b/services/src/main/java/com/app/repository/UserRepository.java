package com.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.app.entity.User;

public interface UserRepository extends CrudRepository<User, String> {

	@Query("select u from User u where u.userStatus= :userStatus and u.role='ROLE_USER'")
	public List<User> findAllByUserStatus(@Param("userStatus") boolean userStatus);

	@Query("select u from User u where u.userName like %:searchString% and u.role='ROLE_USER'")
	public List<User> findAllUserBySearchString(@Param("searchString") String searchString);

}
