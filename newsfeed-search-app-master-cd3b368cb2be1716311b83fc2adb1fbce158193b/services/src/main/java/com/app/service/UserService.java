package com.app.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.app.entity.SearchHistory;
import com.app.entity.SignupStatus;
import com.app.entity.User;
import com.app.repository.SearchHistoryRepository;
import com.app.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {
	Logger logger = LoggerFactory.getLogger(UserService.class);
	@Autowired
	private UserRepository userRepo;
	@Autowired
	PasswordEncoder encoder;
	@Autowired
	private SearchHistoryRepository searchRepo;

	// Signup method
	public SignupStatus registerUser(User user) {
		logger.info("from signup");
		SignupStatus signupStatus = new SignupStatus();
		if (userRepo.existsById(user.getUserEmail())) {

			signupStatus.setSignupStatus(false);
			signupStatus.setErrorMessage("email already exists");
			return signupStatus;
		}
		user.setUserPassword(encoder.encode(user.getUserPassword()));
		userRepo.save(user);
		signupStatus.setSignupStatus(true);
		signupStatus.setErrorMessage("saved");
		return signupStatus;
	}

	// for login purpose loading the user
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		User user = userRepo.findById(username).get();
		List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
		roles.add(new SimpleGrantedAuthority(user.getRole()));
		org.springframework.security.core.userdetails.User user1 = new org.springframework.security.core.userdetails.User(
				user.getUserEmail(), user.getUserPassword(), roles);
		return user1;

	}

	/*
	 * to get all User
	 */
	public List<User> getAllUser() {
		List<User> listAllUser = (List<User>) userRepo.findAll();
		return listAllUser;
	}

	// to Deactivate a particular user
	public String blackListUser(String userEmail) {
		User user = userRepo.findById(userEmail).get();
		user.setUserStatus(false);
		userRepo.save(user);
		return "user blacklisted";
	}

	// to Activate User
	public String activateUser(String userEmail) {
		User user = userRepo.findById(userEmail).get();
		user.setUserStatus(true);
		userRepo.save(user);
		return "user activated";
	}

	/*
	 * author: 730063 to get all activate user
	 */
	public List<User> getAllActivatedUser(boolean userStatus) {
		List<User> allActivatedUser = userRepo.findAllByUserStatus(userStatus);
		return allActivatedUser;
	}

	/*
	 * author ; 730063 to get all deactivated user
	 */
	public List<User> getAllDeactivatedUser(boolean userStatus) {
		logger.info("inside method");
		List<User> alldeactivatedUser = userRepo.findAllByUserStatus(userStatus);
		return alldeactivatedUser;
	}

	/*
	 * to get users searching
	 */
	public List<User> searchUser(String searchUser) {
		List<User> searchUserList = userRepo.findAllUserBySearchString(searchUser);
		return searchUserList;
	}

	/*
	 * to check user account is blocked or not
	 */
	public String isActivate(User user) {
		User appUser = userRepo.findById(user.getUserEmail()).get();
		if (appUser.isUserStatus())
			return "activate";
		return "deactivate";
	}

	public String saveSearchHistory(SearchHistory obj) {
		searchRepo.save(obj);
		return "saved successfully";
	}

	public List<SearchHistory> getHistory(String userEmail) {

		List<SearchHistory> list = searchRepo.findSearchHistoryByUserEmail(userEmail);
		return list;
	}

	public String deleteSearchHistory(int searchHistoryId) throws Exception {
		try {
			searchRepo.deleteById(searchHistoryId);
			return "deleted successfully";
		} catch (Exception e) {
			return "history does not exists from service";
		}
	}

}
