package com.app.controller;

import javax.validation.Valid;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.entity.SignupStatus;
import com.app.entity.User;
import com.app.security.JwtGenerator;
import com.app.service.UserService;

@RestController
@RequestMapping("user")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController extends GlobalErrorHandler {

	Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtGenerator jwtGenerator;

	@PostMapping("registerUser")
	public SignupStatus registerUser(@Valid @RequestBody User user) throws Exception {
		// user.setUserStatus(true);
		logger.info("starting registration from controller");
		return userService.registerUser(user);
	}

	@PostMapping("/login")
	public String login(@RequestBody User appUser) {

		if (userService.isActivate(appUser) == "deactivate")
			return "account is blocked";
		org.springframework.security.core.Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(appUser.getUserEmail(), appUser.getUserPassword()));
		logger.info("from login controller ");
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtGenerator.generateToken(authentication);
		return jwt;
	}

}
