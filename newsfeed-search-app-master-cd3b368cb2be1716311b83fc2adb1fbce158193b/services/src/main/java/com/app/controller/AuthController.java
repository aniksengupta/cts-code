package com.app.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.entity.SearchHistory;
import com.app.entity.User;
import com.app.service.UserService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("common")
public class AuthController extends GlobalErrorHandler {

	Logger logger = LoggerFactory.getLogger(AuthController.class);

	@Autowired
	UserService userService;

	@GetMapping("getAllUser")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<User> getAllUser() {
		logger.info("from get all user method starting");
		List<User> listAllUser = userService.getAllUser();
		logger.info("from get all user method is gonna end");
		return listAllUser;
	}

	// only to blacklist a paticular user
	@PutMapping("blackList/{userEmail}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String blackListUser(@PathVariable("userEmail") String userEmail) {
		logger.info("from blackMail method starting");
		return userService.blackListUser(userEmail);

	}

	// only to activate user
	@PutMapping("activate/{userEmail}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String activateUser(@PathVariable("userEmail") String userEmail) {
		logger.info("from activate user method starting");
		return userService.activateUser(userEmail);
	}

	@GetMapping("getActivateUser")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<User> getAllActivatedUser() {
		logger.info("from get all activate user method");
		boolean userStatus = true;
		List<User> allActivatedUser = userService.getAllActivatedUser(userStatus);
		logger.info("from get all activate user method ending");
		return allActivatedUser;
	}

	@GetMapping("getDeactivateUser")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<User> getAllDeactivatedUser() {
		logger.info("from get all deactivate user method starting");
		boolean userStatus = false;
		List<User> allDeactivatedUser = userService.getAllDeactivatedUser(userStatus);
		logger.info("from get all deactivate user mehtod is gonna end");
		return allDeactivatedUser;
	}

	@GetMapping("searchUser/{searchString}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<User> searchUser(@PathVariable("searchString") String searchString) {
		logger.info("from searchUser method starting");
		return userService.searchUser(searchString);

	}

	@PostMapping("saveSearch")
	@PreAuthorize("hasRole('ROLE_USER')")
	public String saveSearchHistory(@RequestBody SearchHistory obj) {
		logger.info("from save Search method staring");
		return userService.saveSearchHistory(obj);
	}

	@GetMapping("getHistory/{userEmail}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public List<SearchHistory> getHistory(@PathVariable("userEmail") String userEmail) {
		logger.info("from get History by userEmail method staring");
		return userService.getHistory(userEmail);
	}

	@DeleteMapping("deleteHistory/{searchHistoryId}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public String deleteSearchHistory(@PathVariable("searchHistoryId") int searchHistoryId) throws Exception {
		logger.info("from delete method");
		return userService.deleteSearchHistory(searchHistoryId);
	}

}
