package com.app.controller;

import java.util.List;
import java.util.Set;

import javax.security.auth.login.CredentialException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

public class GlobalErrorHandler {

	Logger logger = LoggerFactory.getLogger(GlobalErrorHandler.class);
	String defaultMessage = "";

	@ExceptionHandler(Exception.class)
	// @ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public ResponseEntity<String> handleError(Exception ex) {
		logger.info("In handler" + ex.getMessage());
		if (ex instanceof MethodArgumentNotValidException) {
			defaultMessage = "Error: ";
			MethodArgumentNotValidException exp = (MethodArgumentNotValidException) ex;
			BindingResult bindingResult = exp.getBindingResult();
			List<FieldError> fieldError = bindingResult.getFieldErrors();
			fieldError.stream().forEach(err -> {
				String test = err.getDefaultMessage() + ",\n";
				defaultMessage += test;
			});
			return new ResponseEntity<String>(defaultMessage, HttpStatus.BAD_REQUEST); // .substring(0,
																						// defaultMessage.length()
																						// -
																						// 1);
		}
		if (ex instanceof AuthenticationException)
			return new ResponseEntity<String>("error in auth", HttpStatus.UNAUTHORIZED);
		if (ex instanceof AccessDeniedException)
			return new ResponseEntity<String>("error in auth", HttpStatus.UNAUTHORIZED);
		return new ResponseEntity<String>("else", HttpStatus.BAD_REQUEST);
	}
}
