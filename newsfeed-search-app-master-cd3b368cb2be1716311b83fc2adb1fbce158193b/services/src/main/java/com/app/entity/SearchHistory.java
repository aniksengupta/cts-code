package com.app.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class SearchHistory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int searchHistoryId;

	private String searchTopic;

	@CreationTimestamp
	private LocalDateTime searchTime;

	private String userEmail;

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public int getSearchHistoryId() {
		return searchHistoryId;
	}

	public void setSearchHistoryId(int searchHistoryId) {
		this.searchHistoryId = searchHistoryId;
	}

	public String getSearchTopic() {
		return searchTopic;
	}

	public void setSearchTopic(String searchTopic) {
		this.searchTopic = searchTopic;
	}

	public LocalDateTime getSearchTime() {
		return searchTime;
	}

	public void setSearchTime(LocalDateTime searchTime) {
		this.searchTime = searchTime;
	}

}
