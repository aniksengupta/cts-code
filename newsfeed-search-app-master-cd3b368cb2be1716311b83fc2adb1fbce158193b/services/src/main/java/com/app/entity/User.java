package com.app.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class User {
	@Id
	@NotBlank(message = "please give your email")
	@Email(message = "must give a valid email address")
	private String userEmail;

	@NotBlank(message = "enter name")
	@Pattern(regexp = ("[A-Z]*[a-z]*\\s[A-Z]*[a-z]*{4,}"), message = "can contain only alphabets and only give first name and last name")
	@Size(min = 4, max = 25, message = "min length is 3 and maximum length 25")
	private String userName;

	@NotBlank(message = "enter password")
	@Size(min = 8, message = "min length is 8")
	@Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$", message = "Password must contain at least eight characters, one capital letter, one small letter and at least one number")
	private String userPassword;

	private String role;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	// @NotBlank(message="give true if user is available else give false")
	@JsonProperty
	private boolean userStatus;

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public boolean isUserStatus() {
		return userStatus;
	}

	public void setUserStatus(boolean userStatus) {
		this.userStatus = userStatus;
	}

}
