package com.app.entity;

public class SignupStatus {

	private boolean signupStatus;
	private String errorMessage;

	public boolean isSignupStatus() {
		return signupStatus;
	}

	public void setSignupStatus(boolean signupStatus) {
		this.signupStatus = signupStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public SignupStatus(boolean signupStatus, String errorMessage) {
		this.signupStatus = signupStatus;
		this.errorMessage = errorMessage;
	}

	public SignupStatus() {
	}

	public boolean equals(SignupStatus obj) {
		// TODO Auto-generated method stub
		if (this.signupStatus == obj.signupStatus && this.errorMessage == obj.errorMessage)
			return true;
		else
			return false;
	}

}
