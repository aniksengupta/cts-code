package com.app.controllerTesting;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.app.entity.SearchHistory;
import com.app.entity.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AuthControllerTest {

	@Autowired
	private WebApplicationContext webApplicationContext;
	private MockMvc mockmvc;

	@Before
	public void setUp() {
		this.mockmvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}

	/*
	 * without token trying to access blacklist method , will give unauthorized
	 */
	@Test
	public void blackListUserFail() throws JsonProcessingException, Exception {

		mockmvc.perform(put("/common/blackList/dada@gmail.com").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isUnauthorized());

	}

	/*
	 * setting role as user , then trying to access admin specific method
	 */
	@Test
	@WithMockUser(roles = "USER")
	public void blackListUser() throws Exception {
		mockmvc.perform(put("/common/blackList/dada@gmail.com").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isUnauthorized());

	}

	/*
	 * setting role as admin then trying to access admin specific method
	 */
	@Test
	@WithMockUser(roles = "ADMIN")
	public void blackListUserPass() throws Exception {
		mockmvc.perform(put("/common/blackList/dada@gmail.com").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isOk()).andExpect(content().string(containsString("user blacklisted")));
	}

	/*
	 * test for user. try to delete search history without token
	 */
	@Test
	public void testForDeleteSearchHistoryFail() throws Exception {
		mockmvc.perform(delete("/common/deleteHistory/8").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isUnauthorized());

	}

	/*
	 * trying to access deleteSearchHistory method to delete search history
	 */
	@Test
	@WithMockUser(roles = "USER")
	public void testForDeleteSearchHistory() throws Exception {
		mockmvc.perform(delete("/common/deleteHistory/22")// give one existing
															// id
				.contentType("application/json;charset=UTF-8")).andExpect(status().isOk());

	}

	/*
	 * setting role as admin and trying to access user specific method
	 */
	@Test
	@WithMockUser(roles = "ADMIN")
	public void testForDeleteSearchHistoryFail2() throws Exception {
		mockmvc.perform(delete("/common/deleteHistory/22")// give one existing
															// id
				.contentType("application/json;charset=UTF-8")).andExpect(status().isUnauthorized());

	}

	/*
	 * setting roles as admin and trying to access this getActivateUser method
	 */
	@Test
	@WithMockUser(roles = "ADMIN")
	public void testGetAllActiveUser() throws Exception {
		mockmvc.perform(get("/common/getActivateUser").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isOk());

	}

	/*
	 * with setting role as admin, trying to access getAllActivate() function
	 */
	@Test
	public void testGetAllActiveUserFail() throws Exception {
		mockmvc.perform(get("/common/getActivateUser").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isUnauthorized());

	}

	/*
	 * setting role as user and trying to access admin specific method
	 */
	@Test
	@WithMockUser(roles = "USER")
	public void testGetAllUserFail2() throws Exception {
		mockmvc.perform(get("/common/getActivateUser").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isUnauthorized());

	}

	/*
	 * setting role as user and trying to access admin specific method
	 */
	@Test
	@WithMockUser(roles = "USER")
	public void testForAllDeactiveUserFail() throws Exception {
		mockmvc.perform(get("/common/getDeactivateUser").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isUnauthorized());

	}

	/*
	 * setting role as admin and trying to access admin specific method
	 */
	@Test
	@WithMockUser(roles = "ADMIN")
	public void testForAllDeactiveUserPass() throws Exception {
		mockmvc.perform(get("/common/getDeactivateUser").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isOk());

	}

	/*
	 * setting role as user and trying to access admin specific method
	 */
	@Test
	@WithMockUser(roles = "USER")
	public void testForSearchUserFail() throws Exception {
		mockmvc.perform(get("/common/searchUser/as").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isUnauthorized());

	}

	/*
	 * setting role as admin and trying to access admin specific method
	 */
	@Test
	@WithMockUser(roles = "ADMIN")
	public void testForSearchUserPass() throws Exception {
		mockmvc.perform(get("/common/searchUser/as").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isOk());

	}

	/*
	 * setting role as user and trying to access save history method (user
	 * specific)
	 */
	@Test
	@WithMockUser(roles = "USER")
	public void testForSaveSearchHistory() throws Exception {
		SearchHistory obj = new SearchHistory();
		obj.setSearchTopic("america");
		obj.setUserEmail("saswata@mail.com");
		ObjectMapper obMapper = new ObjectMapper();
		mockmvc.perform(post("/common/saveSearch").content(obMapper.writeValueAsString(obj))
				.contentType("application/json;charset=UTF-8")).andExpect(status().isOk())
				.andExpect(content().string(containsString("saved successfully")));

	}

	/*
	 * setting role as admin and trying to access save history method (user
	 * specific)
	 */
	@Test
	@WithMockUser(roles = "ADMIN")
	public void testForSaveSearchHistoryFail() throws Exception {
		SearchHistory obj = new SearchHistory();
		obj.setSearchTopic("america");
		obj.setUserEmail("saswata@mail.com");
		ObjectMapper obMapper = new ObjectMapper();
		mockmvc.perform(post("/common/saveSearch").content(obMapper.writeValueAsString(obj))
				.contentType("application/json;charset=UTF-8")).andExpect(status().isUnauthorized());

	}

	/*
	 * setting role as admin and trying to access user specific method
	 */
	@Test
	@WithMockUser(roles = "ADMIN")
	public void testForGetHistoryFail() throws Exception {
		mockmvc.perform(get("/common/getHistory/saswata@mail.com").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isUnauthorized());

	}

	/*
	 * setting role as user and trying to access user specific method
	 */
	@Test
	@WithMockUser(roles = "USER")
	public void testForGetHistoryPass() throws Exception {
		mockmvc.perform(get("/common/getHistory/saswata@mail.com").contentType("application/json;charset=UTF-8"))
				.andExpect(status().isOk());

	}

}
