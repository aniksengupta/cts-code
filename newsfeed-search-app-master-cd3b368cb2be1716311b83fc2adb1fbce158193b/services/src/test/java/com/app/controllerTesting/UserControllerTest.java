package com.app.controllerTesting;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.hamcrest.Matchers.*;
import com.app.entity.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserControllerTest {

	@Autowired
	private WebApplicationContext webApplicationContext;
	private MockMvc mockmvc;

	@Before
	public void setup() {

		this.mockmvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}

	/*
	 * to test registration is done on giving everything correct satisfying all
	 * validation
	 */
	@Test
	public void testForSuccessfulSignup() throws Exception {
		User user = new User();
		String mail = getRandomMail(5);// generating random string
		user.setUserEmail(mail + "@mail.com");// setting random mail
		user.setUserName("ssan kaaaa");
		user.setUserPassword("Password@10");
		user.setUserStatus(true);
		user.setRole("ROLE_USER");

		ObjectMapper obj = new ObjectMapper();
		mockmvc.perform(post("/user/registerUser").content(obj.writeValueAsString(user))
				.contentType("application/json;charset=UTF-8")).andExpect(status().isOk());

	}

	/*
	 * does not match all the validation, should give badrequest response
	 */
	@Test
	public void unsuccessfulRegister() throws JsonProcessingException, Exception {
		User user = new User();
		user.setUserEmail("a@a.com");
		user.setUserPassword("asddd");// does not match the pattern
		ObjectMapper obj = new ObjectMapper();
		mockmvc.perform(post("/user/registerUser").content(obj.writeValueAsString(user))
				.contentType("application/json;charset=UTF-8")).andExpect(status().isBadRequest());

	}

	/*
	 * admin login on giving correct userName password
	 */
	@Test
	public void TestForLogin() throws JsonProcessingException, Exception {
		User user = new User();
		user.setUserEmail("sudip@gmail.com");
		user.setUserPassword("Anik123@");
		ObjectMapper obj = new ObjectMapper();
		mockmvc.perform(
				post("/user/login").content(obj.writeValueAsString(user)).contentType("application/json;charset=UTF-8"))
				.andExpect(status().isOk());
	}

	/*
	 * on giving wrong user name or password
	 */
	@Test
	public void testForLoginFail() throws Exception {
		User user = new User();
		user.setUserEmail("sudiop@gmail.com");
		user.setUserPassword("Anik123@");
		ObjectMapper obj = new ObjectMapper();
		mockmvc.perform(
				post("/user/login").content(obj.writeValueAsString(user)).contentType("application/json;charset=UTF-8"))
				.andExpect(status().isBadRequest());
	}

	/*
	 * for email exists testing on giving existing id while registration
	 */
	@Test
	public void emailExistsRegistration() throws Exception {
		User user = new User();
		user.setUserEmail("saswata@mail.com");
		user.setUserName("sa ka");
		user.setUserPassword("Password@10");
		user.setUserStatus(true);
		user.setRole("ROLE_USER");
		ObjectMapper obMapper = new ObjectMapper();
		mockmvc.perform(post("/user/registerUser").content(obMapper.writeValueAsString(user))
				.contentType("application/json;charset=UTF-8")).andExpect(status().isOk())
				.andExpect(jsonPath("$.errorMessage", is("email already exists")));

	}

	static String getRandomMail(int n) {
		String mailString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvxyz";
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) {

			int index = (int) (mailString.length() * Math.random());
			sb.append(mailString.charAt(index));
		}

		return sb.toString();
	}
}
