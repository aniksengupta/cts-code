package com.app.serviceTesting;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.app.entity.SearchHistory;
import com.app.entity.SignupStatus;
import com.app.entity.User;
import com.app.repository.SearchHistoryRepository;
import com.app.repository.UserRepository;
import com.app.service.UserService;

public class ServiceTesting {

	@InjectMocks
	private UserService userService;
	@Mock
	private UserRepository userRepo;
	@Mock
	private PasswordEncoder encoder;
	@Mock
	private SearchHistoryRepository searchRepo;

	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
	}

	/*
	 * registerUser() testing successful
	 */
	@Test
	public void successfulSignup() {
		User user = new User();
		user.setUserEmail("aaa@mail.com");
		user.setUserName("Anik sengupta");
		user.setUserPassword("a1233Anik@");
		user.setRole("ROLE_ADMIN");
		user.setUserStatus(true);
		when(userRepo.existsById(user.getUserEmail())).thenReturn(false);
		when(encoder.encode(user.getUserPassword())).thenReturn(user.getUserPassword());
		userService.registerUser(user);
		Mockito.verify(userRepo, times(1)).save(user);

	}

	/*
	 * registerUser() testing for failed cause email exists
	 */
	@Test
	public void signupFailsForEmailExists() {
		User user = new User();
		user.setUserEmail("aaa@mail.com");
		user.setUserName("Anik sengupta");
		user.setUserPassword("a1233Anik@");
		user.setRole("ROLE_USER");
		user.setUserStatus(true);
		when(userRepo.existsById(user.getUserEmail())).thenReturn(true);
		SignupStatus status = userService.registerUser(user);
		SignupStatus expectedStatus = new SignupStatus(false, "email already exists");
		assertEquals(true, status.equals(expectedStatus));
	}

	/*
	 * successful test for blacklist user
	 */
	@Test
	public void testForBlacklistUser() {
		User user = new User();
		user.setUserEmail("jack@gmail.com");
		user.setUserName("Anik sengupta");
		user.setUserPassword("a1233Anik@");
		user.setRole("ROLE_USER");
		user.setUserStatus(true);
		when(userRepo.findById("jack@gmail.com")).thenReturn(Optional.of(user));
		when(userRepo.save(user)).thenReturn(user);
		assertEquals("user blacklisted", userService.blackListUser("jack@gmail.com"));
	}

	/*
	 * test for all activated user
	 */
	@Test
	public void testForGetAllActivatedUser() {
		List<User> list = new ArrayList<User>();
		when(userRepo.findAllByUserStatus(true)).thenReturn(list);
		assertEquals(list, userService.getAllActivatedUser(true));
	}

	/*
	 * test for fail case to get all activated user
	 */
	@Test
	public void testForGetAllActivatedUserFail() {
		when(userRepo.findAllByUserStatus(true)).thenReturn(null);
		assertEquals(null, userService.getAllActivatedUser(true));
	}

	/*
	 * for searching the users by giving name
	 */
	@Test
	public void testForSearchUserList() {
		List<User> list = new ArrayList<User>();
		when(userRepo.findAllUserBySearchString("as")).thenReturn(list);
		assertEquals(list, userService.searchUser("as"));
	}

	/*
	 * search user with no values
	 */
	@Test
	public void testForSearchUserListFail() {
		when(userRepo.findAllUserBySearchString("")).thenReturn(null);
		assertEquals(null, userService.searchUser(""));
	}

	/*
	 * test for isActivate() successful
	 */
	@Test
	public void testForIsActivate() {
		String expectedResult = "activate";
		User user = new User();
		user.setUserEmail("jack@gmail.com");
		user.setUserName("Anik sengupta");
		user.setUserPassword("a1233Anik@");
		user.setRole("ROLE_USER");
		user.setUserStatus(true);
		when(userRepo.findById(user.getUserEmail())).thenReturn(Optional.of(user));
		String result = userService.isActivate(user);
		assertEquals(true, result == expectedResult);
	}

	/*
	 * test for isActivate() fail cause
	 */
	@Test
	public void testForIsActivateFail() {
		User user = new User();
		user.setUserEmail("jack@gmail.com");
		user.setUserName("Anik sengupta");
		user.setUserPassword("a1233Anik@");
		user.setRole("ROLE_USER");
		user.setUserStatus(false);
		String expectedResult = "deactivate";
		when(userRepo.findById(user.getUserEmail())).thenReturn(Optional.of(user));
		String result = userService.isActivate(user);
		assertEquals(true, result == expectedResult);
	}

	/*
	 * save search history test case
	 */
	@Test
	public void savesearchHistory() {
		SearchHistory obj = new SearchHistory();
		String result = "saved successfully";
		when(searchRepo.save(obj)).thenReturn(obj);
		assertEquals(result, userService.saveSearchHistory(obj));
	}

	/*
	 * get the search history
	 */
	@Test
	public void getHistory() {
		List<SearchHistory> list = new ArrayList<SearchHistory>();
		when(searchRepo.findSearchHistoryByUserEmail("aaa@mail.com")).thenReturn(list);
		assertEquals(list, userService.getHistory("aaa@mail.com"));
	}

	/*
	 * delete search history returns successful
	 */
	@Test
	public void deleteSearchHistory() throws Exception {
		String expectedResult = "deleted successfully";
		assertEquals(expectedResult, userService.deleteSearchHistory(1));
	}

}
