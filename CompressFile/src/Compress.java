import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

// for this program you need to have that file in that specific directory
public class Compress {
	
	public void zipFile() throws FileNotFoundException,IOException {
		
		byte[] buffer = new byte[1024];
		FileOutputStream fos = new FileOutputStream("D:\\Preparation\\StudentsZip.zip"); //this will create the output compress folder
		
		ZipOutputStream zos = new ZipOutputStream(fos);
		
		ZipEntry ze = new ZipEntry("StudentsAfterZip.xlsx"); //inside the compress folder the file name you want ..... inside file name
		
		zos.putNextEntry(ze);
		
		FileInputStream fis = new FileInputStream("D:\\Preparation\\students.xlsx"); //input file name
		
		int len;
		while((len=fis.read(buffer)) >0){
			zos.write(buffer, 0, len);
		}
		
		fis.close();
		zos.closeEntry();
		zos.close();
		System.out.println("zipping done");
	}
}
