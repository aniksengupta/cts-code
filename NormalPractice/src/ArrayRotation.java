import java.util.Scanner;

public class ArrayRotation {

	public void rotateArray() {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the size ");
		int size = sc.nextInt();
		int[] arr = new int[size];
		
		for(int i=0;i<size;i++) {
			System.out.println("enter elements ");
			arr[i]=sc.nextInt();
		}
		
		System.out.println("number of time you want to rotate");
		int k = sc.nextInt();
		
		
		int[] shiftedArray = rotate(arr, k);
		System.out.println("after rotation = ");
		for(int i=0;i<shiftedArray.length;i++) {
			System.out.print(shiftedArray[i]+" ");
		}
			
	}
	
	static int[] rotate(int[] arr,int k) {
		if(k==0) {
			return arr;
		}
		if(arr.length==1) {
			return arr;
		}
		
			int pos = -1;
			int[] shiftedArray = new int[arr.length];
			if(k > arr.length) {
				k = k % arr.length;
			}
			for(int i=0;i<arr.length;i++) {
				
				if((i+k)>arr.length-1) {
					pos = Math.abs(arr.length-i-k);
					System.out.println("from if pos = "+pos);
				}else {
					pos = i+k;
					System.out.println("else pos = "+pos);
				}
				
				shiftedArray[pos]=arr[i];
			}
		
		return shiftedArray;
		
		
		
	}
	
	public void fibonacci(int range) {
		System.out.println("breaking the line");
		int[] arr = new int[range];
		arr[0]=0;
		arr[1]=1;
		for(int i=2;i<range;i++) {
			int f= arr[i-1];
			int se=arr[i-2];
			arr[i] = f+se;
		}
		
		for(int i=0;i<arr.length;i++) {
			System.out.print(arr[i]+" ");
		}
	}
	
	
	public boolean checkPalindrome(String s) {
		String r="";
		for(int i=s.length()-1;i>=0;i--) {
			r=r+Character.toString(s.charAt(i));
		}
		
		if(s.equals(r)) {
			return true;
		}else {
			return false;
		}
	}
	
	
	// l -left  r-right -- x-value to be search
	int binarySearch(int arr[], int l, int r, int x) 
    { 
        if (r >= l) { 
            int mid = l + (r - l) / 2; 
  
            // If the element is present at the 
            // middle itself 
            if (arr[mid] == x) 
                return mid; 
  
            // If element is smaller than mid, then 
            // it can only be present in left subarray 
            if (arr[mid] > x) 
                return binarySearch(arr, l, mid - 1, x); 
  
            // Else the element can only be present 
            // in right subarray 
            return binarySearch(arr, mid + 1, r, x); 
        } 
  
        // We reach here when element is not present 
        // in array 
        return -1; 
    } 
	
	
}
