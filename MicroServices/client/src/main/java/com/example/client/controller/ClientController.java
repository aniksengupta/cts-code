package com.example.client.controller;

import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.example.client.resources.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.ribbon.proxy.annotation.Hystrix;

@RestController
@RequestMapping("/client")
public class ClientController {
	final String url = "http://DEMO-SERVER/server";
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/hello")
	public ResponseEntity<?> helloFromClient() {
		return new ResponseEntity<String>("good morning from client side",
				HttpStatus.OK);
	}

	/*
	 * it will return a simple text from another microservices author : 730063
	 */
	@HystrixCommand(fallbackMethod = "fallback")
	@GetMapping("/server")
	public ResponseEntity<?> helloFromServer() {
		String message = restTemplate.getForObject(this.url + "/hello",
				String.class);
		return new ResponseEntity<String>(message, HttpStatus.OK);
	}

	/*
	 * it will return an user object from another microservices author : 730063
	 */
	@HystrixCommand(fallbackMethod = "fallback")
	@GetMapping("/users")
	public ResponseEntity<?> getUser() {
		User user = (User) restTemplate.getForObject(this.url + "/user", User.class);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/*
	 * returns a list of users from another microservice
	 */
	@HystrixCommand(fallbackMethod = "fallback")
	@GetMapping("/allUser")
	public ResponseEntity<?> getUsers() {
		List<User> userList;
		userList = (List<User>) restTemplate.getForObject(
				this.url + "/allUser", Collection.class);
		return new ResponseEntity<List<User>>(userList, HttpStatus.OK);

	}

	// public User fallbackUser() {
	// return new User("default","default",0);
	// }
	public ResponseEntity<?> fallback() {
		return new ResponseEntity<String>("fall back. try after some time",
				HttpStatus.OK);
	}
}
