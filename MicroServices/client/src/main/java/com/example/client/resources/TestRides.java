package com.example.client.resources;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TestRides {
	@Id
	private int id;
	private String car_name;
	private String date;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the car_name
	 */
	public String getCar_name() {
		return car_name;
	}
	/**
	 * @param car_name the car_name to set
	 */
	public void setCar_name(String car_name) {
		this.car_name = car_name;
	}
	
	
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

}
