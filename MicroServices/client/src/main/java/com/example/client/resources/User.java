package com.example.client.resources;

import javax.persistence.Entity;
import javax.persistence.Id;


public class User {
	
	private String name;
	private String gender;
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public User() {

	}

	public User(String name, String gender, int id) {
		this.name = name;
		this.gender = gender;
		this.age = age;
	}
}
