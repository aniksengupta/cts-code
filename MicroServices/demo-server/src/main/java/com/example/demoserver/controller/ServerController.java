package com.example.demoserver.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoserver.resources.User;

@RestController
@RequestMapping("/server")
public class ServerController {

	@GetMapping("/morning")
	public String goodMorning() {
		return "good morning from server";
	}

	@GetMapping("/hello")
	public String hello() {
		return "hello from server side on the call from client side";
	}

	@GetMapping("/user")
	public User sendUser() {
		User user = new User("john", "male", 24);
		return user;
	}
	
	@GetMapping("/allUser")
	public List<User> getAllUsers(){
		List<User> userList= new ArrayList();
		User user1= new User("john","gender",23);
		User user2= new User("Lue","gender",25);
		userList.add(user1);
		userList.add(user2);
		return userList;
	}

}
