
public class Child extends Parent{
	
	public void multiply() {
		System.out.println("from void type");
	}
	
	
	public int add(int a,int b) {
		System.out.println("overriding "+(a+b));
		return a+b;
	}
	
	public static void display(int a) {
		System.out.println("from child static method "+a);
	}

	public final void methodFinal(int a) {
		System.out.println("from final method child");
	}
}
