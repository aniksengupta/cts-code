
public class InheritanceMain {

	public static void main(String args[]) {
		
		Child c = new Child();
		c.multiply();
		c.multiply(2,3);
		c.add(5,5);
		
		c.display();
		c.display(2);
		
	}
}
