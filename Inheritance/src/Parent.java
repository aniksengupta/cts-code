
public class Parent {
	
	public int add(int a,int b) throws Exception{
		return (a+b);
	}
	
	public void multiply(int a,int b) {
		System.out.println("multiplication : "+a*b);
	}
	
	public static void display() {
		System.out.println("from parent class static method");
	}
	
	public final void methodFinal() {
		System.out.println("from final method from parent");
	}
}
