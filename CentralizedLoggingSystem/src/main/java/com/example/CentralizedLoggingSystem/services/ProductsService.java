package com.example.CentralizedLoggingSystem.services;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import com.example.CentralizedLoggingSystem.entity.Products;

@Service
public class ProductsService {

	
	public void createFile() throws FileNotFoundException {
		System.out.println("from create file method");
		List<Products> pList = getProducts();
		
		File file = new File("D:/sample.csv");
		PrintWriter printWriter = new PrintWriter(new FileOutputStream(file));
		printWriter.println(renderRecords(pList));
		printWriter.flush();
		printWriter.close();

	}
	
	
	String renderRecords(List<Products> pList) {
		System.out.println("from render method");
	    STGroup st = new STGroupFile("templates/setup-amend-dd-template.stg");
	    ST orderRecord = st.getInstanceOf("setProducts");
	    orderRecord.add("pList", pList);
	    return orderRecord.render();
	}

	
	
	
	public List<Products> getProducts(){
		System.out.println("from getProduct method");
		Products p1 = new Products(1,"p1",100,"s1");
		Products p2 = new Products(1,"p2",100,"s2");
		Products p3 = new Products(1,"p3",100,"s3");
		Products p4 = new Products(1,"p4",100,"s4");
		List<Products> pList = new ArrayList<>();
		pList.add(p1);pList.add(p2);pList.add(p3);pList.add(p4);
		return pList;
	}
	
}
