package com.example.CentralizedLoggingSystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.CentralizedLoggingSystem.entity.Book;
import com.example.CentralizedLoggingSystem.repository.BookRepository;

@Service
public class BookServices {
	
	@Autowired
	BookRepository bookRepo;
	
	public BookServices() {
		System.out.println("creating object of Book Service");
		
	}
	
	public Book getBooksById(String bookId) {
		return bookRepo.findBookByBookId(bookId);
	}
	
	public List<Book> findAllBooks(){
		return (List<Book>) bookRepo.findAll();
	}
	
	public Book saveBook(Book book) {
		return bookRepo.save(book);
	}
	

}
