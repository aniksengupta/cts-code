package com.example.CentralizedLoggingSystem.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Book {
	
	String bookName;
	String author;
	
	@Id
	String bookId;
	
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	
	

}
