package com.example.CentralizedLoggingSystem.entity;

public class Products {
	
	private int id;
	private String name;
	private int price;
	private String status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Products() {}
	
	public Products(int id, String name, int price, String status) {
		
		this.id = id;
		this.name = name;
		this.price = price;
		this.status = status;
	}
	
	

}
