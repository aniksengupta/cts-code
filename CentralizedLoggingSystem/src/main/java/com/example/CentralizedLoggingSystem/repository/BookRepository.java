package com.example.CentralizedLoggingSystem.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.CentralizedLoggingSystem.entity.Book;

public interface BookRepository extends CrudRepository<Book, String>{

	Book findBookByBookId(String bookId);
	
}
