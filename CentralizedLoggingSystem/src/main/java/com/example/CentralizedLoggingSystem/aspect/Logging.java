//package com.example.CentralizedLoggingSystem.aspect;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.After;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.aspectj.lang.annotation.Pointcut;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//@Aspect
//@Component
//public class Logging {
//
//	Logger log = LoggerFactory.getLogger(Logging.class);
//	ObjectMapper mapper = new ObjectMapper();
//
//	@Pointcut(value = "execution(* com.example.CentralizedLoggingSystem.*.*.*(..) )")
//	public void getPointCut() {
//
//	}
//
//	@Before("getPointCut()")
//	public void beforeEachMethod() {
//		log.info("Starting....");
//	}
//
//	@Around("getPointCut()")
//	public Object logger(ProceedingJoinPoint pjp) throws Throwable {
//		String methodName = pjp.getSignature().getName();
//		String className = pjp.getTarget().getClass().toString();
//		
//		Object[] input = pjp.getArgs();
//		
//		log.info("from class : " + className + "  method : " + methodName + " -- input arguments : "
//				+ mapper.writeValueAsString(input));
//		
//		Object object = pjp.proceed();
//
//		log.info("from class : " + className + "  method : " + methodName + " -- output results : "
//				+ mapper.writeValueAsString(object));
//		return object;
//	}
//
//	@After("getPointCut()")
//	public void afterEachMethod() {
//		log.info("ending");
//	}
//
//}
