package com.example.CentralizedLoggingSystem.controller;

import java.io.FileNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.CentralizedLoggingSystem.services.ProductsService;

@RestController
@RequestMapping("/products")
public class ProductsController {

	@Autowired ProductsService pService;
	
	@GetMapping
	public void createCSVFile() throws FileNotFoundException {
		System.out.println("from controller");
		pService.createFile();
	}
	
	
}
