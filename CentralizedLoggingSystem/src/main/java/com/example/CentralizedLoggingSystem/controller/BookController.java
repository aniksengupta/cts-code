package com.example.CentralizedLoggingSystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.CentralizedLoggingSystem.entity.Book;
import com.example.CentralizedLoggingSystem.services.BookServices;

@RestController
@RequestMapping("/book")
public class BookController {

	@Autowired
	BookServices bookService;

	public BookController() {
		System.out.println("creating object of BookController");
	}

	@GetMapping
	List<Book> getAllBooks() {
		System.out.println("getAll");
		return bookService.findAllBooks();

	}

	@GetMapping("{bookId}")
	public Book getBookById(@PathVariable("bookId") String bookId) {
		return bookService.getBooksById(bookId);
	}

	@PostMapping
	Book insertBook(@RequestBody Book book) {
		return bookService.saveBook(book);
	}

}
