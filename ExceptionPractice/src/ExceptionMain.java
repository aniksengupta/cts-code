import java.sql.SQLException;

public class ExceptionMain {

	@SuppressWarnings("finally")
	public static void main(String str[]) throws Exception   {
		System.out.println("from main class");
		try {
			System.out.println("from try block ");
			int i=10/0;
			System.out.println("after dividing by zero");
		}
		catch(ArithmeticException ex) {
			System.out.println("in Arith catch block");
		}
		catch(Exception ex) {
			System.out.println("in catch block");
		}
		
		finally {
			System.out.println("before thworing exception from finally block");
			try {
			throw new ArithmeticException();
			}catch(ArithmeticException ex) {
				System.out.println("hh");
			}
			System.out.println(" after thworing exception from finally block");
			System.out.println("d");
		}
		System.out.println("d");

		System.out.println("sd");
	}
}
