import java.util.ArrayList;
import java.util.List;

public class PrimeCheck {

	
	public boolean checkPrime(int num) {
		boolean res= true;
		for(int i=2;i<=num-1;i++) {
			if(num%i==0) {
				res = false;
				break;
			}
		}
		return res;
	}
	
	
	public int countOfBalloon(String str) {
		List<String> word = new ArrayList<>();
		
		for(int i=0;i<str.length();i++) {
			word.add(Character.toString(str.charAt(i)));
		}
		
		int count =0;
		while(word.size()>=7) {
		boolean present = isBalloonPresent(word);
		if(present==true) {
			try {
			word.remove(word.indexOf("B"));
			word.remove(word.indexOf("A"));
			word.remove(word.indexOf("L"));
			word.remove(word.indexOf("L"));
			word.remove(word.indexOf("O"));
			word.remove(word.indexOf("O"));
			word.remove(word.indexOf("N"));
			count=count+1;
			
			}catch(Exception ex) {
				System.out.println("exception");
				return count;
			}
		}
		else {
			return count;
		}
		
		}
		return count;
	}
	
	public static boolean isBalloonPresent(List<String> word) {
		if(word.contains("B")&&word.contains("B")&&word.contains("A")&&word.contains("L")&&word.contains("L")&&word.contains("O")&&word.contains("N")) {
			return true;
		}else {
			return false;
		}
	}
	
}
