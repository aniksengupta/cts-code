package com.stackroute.favouriteservice.repositorytest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.stackroute.favouriteservice.domain.Article;
import com.stackroute.favouriteservice.repository.NewsRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class NewsRepoTest {

	@Autowired
	private NewsRepository repo;
	 private Article article;
	 private Article article1;
	 private Article article2;
	 
	 @Before
		public void setup() {
		 article = new Article("test","test","test","test","test","test","test");
		 article1 = new Article("test1","test1","test1","test1","test1","test1","test1");
		 article1 = new Article("test2","test2","test2","test2","test2","test2","test2");
		}
	 @After
		public void tearDown() {
			System.out.println("from after repo test");
			repo.deleteAllInBatch();
		}

	 	@Test
		public void testSaveNews() throws Exception {
			Article articleObj=repo.save(article);
			final Article articleObj1 = repo.getOne(1);
			assertThat(articleObj1.getId()).isEqualTo(1);
		}
	 	
	 	@Test
		public void testDeleteNews() throws Exception {
			final Article articleObj = repo.save(new Article("test","test","test","test","test","test","test"));
			assertEquals("test", articleObj.getTitle());
			repo.delete(articleObj);
			assertEquals(Optional.empty(), repo.findById(123));	
		}
	 	
	 	@Test
		public void testGetMyNews() {
			List<Article> articleObj = new ArrayList<Article>();
			assertEquals(articleObj, repo.findByUserId("user1"));
			
		}

}
