package com.stackroute.favouriteservice.servicetest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.stackroute.favouriteservice.domain.Article;
import com.stackroute.favouriteservice.exception.NewsAlreadyExist;
import com.stackroute.favouriteservice.exception.NewsNotFound;
import com.stackroute.favouriteservice.repository.NewsRepository;
import com.stackroute.favouriteservice.service.NewsServiceImpl;



public class serviceImplTest {

	//private static final Object Movie = null;

	@Mock
	private NewsRepository repo;

	@InjectMocks
	private NewsServiceImpl newsService;

	private transient Article article;
	transient Optional<Article> options;

	@Before
	public void setUpMock() {
		MockitoAnnotations.initMocks(this);
		article = new Article("test","test","test","test","test","test","test");
		options = Optional.of(article);
	}
	
	@After
	public void tearDown() {
		System.out.println("from after service test");
		repo.deleteAllInBatch();
	}
	/*
	 * test for successful news save
	 */
	@Test
	public void testSaveNewsSuccess() throws NewsAlreadyExist {
		boolean flag = newsService.saveNews(article);
		verify(repo, times(1)).save(article);
	}	
	/*
	 * test for delete by id pass
	 */
	@Test
	public void testDeleteNewsByIdPass() throws NewsNotFound {
		Article articleObj = new Article();
		articleObj.setId(5);
		Optional<Article> newsOpt = Optional.of(articleObj);
		when(repo.existsById(articleObj.getId())).thenReturn(true);
		assertEquals(true, newsService.deleteNewsById(articleObj.getId()));
	}

	/*
	 * testing to delete and giving another id
	 */
	@Test(expected = NewsNotFound.class)
	public void testDeleteNewsByIdFail() throws NewsNotFound {
		Article articleObj = new Article();
		articleObj.setId(5);
		when(repo.existsById(articleObj.getId())).thenReturn(false);
		newsService.deleteNewsById(45);// sending another id

	}
/*
 * test for get my movies
 */
	@Test
	public void testGetMyNews() throws NewsNotFound {
		List<Article> articleObj = new ArrayList<Article>();
		when(repo.findByUserId("user1")).thenReturn(articleObj);
		assertEquals(articleObj, newsService.getMyNews("user1"));
	}

	

}
