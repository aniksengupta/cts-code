package com.stackroute.favouriteservice.controllertest;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stackroute.favouriteservice.controller.NewsController;
import com.stackroute.favouriteservice.domain.Article;
import com.stackroute.favouriteservice.service.NewsServiceImpl;


@RunWith(SpringRunner.class)
@WebMvcTest(NewsController.class)
public class controllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private NewsServiceImpl newsService;
	private Article article1;
	private Article article2;
	private List<Article> list;
	private List<Article> list1;

	@Before
	public void setup() {
		list = new ArrayList<Article>();
		list1 = new ArrayList<Article>();
		article1 = new Article();
		article1.setId(1);
		article1.setTitle("sports");
	
		article2 = new Article();
		article2.setId(2);
		article2.setTitle("news");
		list.add(article2);
	}

	/*
	 * test for add news pass
	 */
	@Test
	public void SaveNewsPass() throws Exception {
		//String token1="eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ1c2VyMSIsImlhdCI6MTU1MjczMjI4MX0.Gol0WujNSNbqh-Vw3H1L2Nhsblcwg5iazmUu1BMKU8F5n3MUfZVlQX2Nhjfhf";
		String token = "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ1c2VyMDAxIiwiaWF0IjoxNTUzMDg0NDQ3fQ.aTGADkz7aU9lurXnLcK0OGcLiQUhjGhhdqrI9pjCCgYHeZj645MsK5XT6iQDOtFI";
		ObjectMapper objectMapper = new ObjectMapper();
		when(newsService.saveNews(article1)).thenReturn(true);
		mockMvc.perform(post("/news/saveNews").content(objectMapper.writeValueAsString(article1)).header("authorization", "Bearer " + token)
				.contentType("application/json;charset=UTF-8")).andExpect(status().isCreated());
		verify(newsService, times(1)).saveNews(Mockito.any(Article.class));
		verifyNoMoreInteractions(newsService);

	}

	/*
	 * test for delete news by id
	 */
	@Test
	public void SuccessfullDeleteNewsById() throws JsonProcessingException, Exception {
		String token="eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ1c2VyMDAxIiwiaWF0IjoxNTUzMDg0NDQ3fQ.aTGADkz7aU9lurXnLcK0OGcLiQUhjGhhdqrI9pjCCgYHeZj645MsK5XT6iQDOtFI";
		when(newsService.deleteNewsById(article1.getId())).thenReturn(true);
		mockMvc.perform(delete("/news/deleteNews/1").header("authorization", "Bearer " + token)).andExpect(status().isOk());
		verify(newsService, times(1)).deleteNewsById(article1.getId());
		verifyNoMoreInteractions(newsService);
	}

	/*
	 * test for get all news
	 */
	@Test
	public void testGetAllNewsPass() throws JsonProcessingException, Exception {
		List<Article> myList=new ArrayList<Article>();
		Article testnews= new Article("test","test","test","test","test","test","test");
		myList.add(testnews);
		String token ="eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ1c2VyMDAxIiwiaWF0IjoxNTUzMDg0NDQ3fQ.aTGADkz7aU9lurXnLcK0OGcLiQUhjGhhdqrI9pjCCgYHeZj645MsK5XT6iQDOtFI";
		when(newsService.getMyNews("user001")).thenReturn(myList);
		mockMvc.perform(get("/news/getMyNews").header("authorization", "Bearer " + token)).andExpect(status().isOk());
		verify(newsService, times(1)).getMyNews("user001");
		verifyNoMoreInteractions(newsService);
	}
}
