package com.stackroute.favouriteservice.exception;

public class NewsAlreadyExist extends Exception{
	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public NewsAlreadyExist(final String message) {
		super(message);
		this.message = message;
	}

	@Override
	public String toString() {
		return "NewsAlreadyExist [message=" + message + "]";
	}

}
