package com.stackroute.favouriteservice.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.stackroute.favouriteservice.domain.Article;
import com.stackroute.favouriteservice.exception.NewsAlreadyExist;
import com.stackroute.favouriteservice.exception.NewsNotFound;

@Service
public interface NewsService {
	
	boolean saveNews(Article article) throws NewsAlreadyExist;
	boolean deleteNewsById(int id) throws NewsNotFound;
	List<Article> getMyNews(String userId) throws NewsNotFound;
}
