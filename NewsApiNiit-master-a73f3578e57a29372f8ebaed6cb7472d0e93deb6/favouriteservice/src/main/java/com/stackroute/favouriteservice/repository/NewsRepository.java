package com.stackroute.favouriteservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stackroute.favouriteservice.domain.Article;


public interface NewsRepository extends JpaRepository<Article, Integer>{
	
	Article findByTitleAndUserId(String title,String userId);
	List<Article> findByUserId(String userId);

}
