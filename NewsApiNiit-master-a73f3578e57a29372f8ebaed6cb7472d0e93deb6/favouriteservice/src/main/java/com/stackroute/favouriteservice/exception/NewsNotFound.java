package com.stackroute.favouriteservice.exception;

public class NewsNotFound extends Exception {

	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public NewsNotFound(final String message) {
		super(message);
		this.message = message;
	}

	@Override
	public String toString() {
		return "NewsNotFound [message=" + message + "]";
	}

}
