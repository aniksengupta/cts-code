package com.stackroute.favouriteservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stackroute.favouriteservice.domain.Article;
import com.stackroute.favouriteservice.exception.NewsAlreadyExist;
import com.stackroute.favouriteservice.exception.NewsNotFound;
import com.stackroute.favouriteservice.repository.NewsRepository;

@Service
public class NewsServiceImpl implements NewsService {

	@Autowired
	private NewsRepository newsRepo;

	@Override
	public boolean saveNews(Article article) throws NewsAlreadyExist {
		
		final Article actualArticle = newsRepo.findByTitleAndUserId(article.getTitle(), article.getUserId());
		if (actualArticle != null) {
			System.out.println("it us null");
			throw new NewsAlreadyExist("Could not mark the article as favourite, Article already exists");
		}
		newsRepo.save(article);
		return true;

	}

	@Override
	public boolean deleteNewsById(int id) throws NewsNotFound {
		
		if (!newsRepo.existsById(id)) {
			throw new NewsNotFound("could not find movie, movie not found");
		}
		newsRepo.deleteById(id);
		return true;

	}

	@Override
	public List<Article> getMyNews(String userId) throws NewsNotFound{
		
		List<Article> newsList = (List<Article>) newsRepo.findByUserId(userId);
		return newsList;
	}

}
