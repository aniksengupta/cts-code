package com.stackroute.favouriteservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.stackroute.favouriteservice.domain.Article;
import com.stackroute.favouriteservice.exception.NewsAlreadyExist;
import com.stackroute.favouriteservice.exception.NewsNotFound;
import com.stackroute.favouriteservice.service.NewsService;
import io.jsonwebtoken.Jwts;

@RestController
@CrossOrigin()
@RequestMapping("news")
public class NewsController {
	
	@Autowired
	private NewsService newsService;
	
	@PostMapping("saveNews")
	public ResponseEntity<?> saveMovie(@RequestBody Article article, final HttpServletRequest request,
			final HttpServletResponse response) {
		final String authHeader = request.getHeader("authorization");
		final String token = authHeader.substring(7);
		String userId = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody().getSubject();
		ResponseEntity<?> responseEntity;
		try {
			article.setUserId(userId);
			newsService.saveNews(article);
			responseEntity = new ResponseEntity<String>("news added", HttpStatus.CREATED);
		} catch (NewsAlreadyExist ex) {
			responseEntity = new ResponseEntity<String>(ex.getMessage(), HttpStatus.CONFLICT);
		} catch (Exception ex) {
			responseEntity = new ResponseEntity<String>("internal server error. please try after some time", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@DeleteMapping("deleteNews/{id}")
	public ResponseEntity<?> deleteMovie(@PathVariable("id") int id) {
		ResponseEntity responseEntity;
		try {
			newsService.deleteNewsById(id);
			responseEntity = new ResponseEntity<String>("news deleted", HttpStatus.OK);
		} catch (NewsNotFound ex) {
			responseEntity = new ResponseEntity<String>(ex.getMessage(), HttpStatus.NOT_FOUND);
		}catch (Exception ex) {
			responseEntity = new ResponseEntity<String>("internal server error. please try after some time", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("getMyNews")
	public ResponseEntity<?> getAllMovies(final HttpServletRequest request, final HttpServletResponse response) {
		ResponseEntity responseEntity;
		final String authHeader = request.getHeader("authorization");
		final String token = authHeader.substring(7);
		String userId = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody().getSubject();
		System.out.println(userId);
		try {
			List<Article> newsList = newsService.getMyNews(userId);
			System.out.println(newsList.size());
			responseEntity = new ResponseEntity<List<Article>>(newsList, HttpStatus.OK);
		} catch (NewsNotFound ex) {
			responseEntity = new ResponseEntity<String>(ex.getMessage(),HttpStatus.NOT_FOUND);
		}catch (Exception ex) {
			responseEntity = new ResponseEntity<String>("internal server error. please try after some time", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

}
