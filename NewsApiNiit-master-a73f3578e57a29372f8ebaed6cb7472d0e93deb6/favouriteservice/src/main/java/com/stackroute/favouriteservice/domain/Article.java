package com.stackroute.favouriteservice.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="favourite_article")
public class Article {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	
	private String userId;

	private String title;

	@Size(max = 350)
	private String description;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(String publishedAt) {
		this.publishedAt = publishedAt;
	}

	public String getSourceWebsiteName() {
		return sourceWebsiteName;
	}

	public void setSourceWebsiteName(String sourceWebsiteName) {
		this.sourceWebsiteName = sourceWebsiteName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlToImage() {
		return urlToImage;
	}

	public void setUrlToImage(String urlToImage) {
		this.urlToImage = urlToImage;
	}

	private String publishedAt;

	private String sourceWebsiteName;

	@Size(max = 350)
	private String url;

	@Size(max = 500)
	private String urlToImage;

	public Article(){
		
	}
	
	public Article(String userId, String title, @Size(max = 350) String description,
			String publishedAt, String sourceWebsiteName, @Size(max = 350) String url,
			@Size(max = 500) String urlToImage) {
		super();
		this.userId = userId;
		this.title = title;
		this.description = description;
		this.publishedAt = publishedAt;
		this.sourceWebsiteName = sourceWebsiteName;
		this.url = url;
		this.urlToImage = urlToImage;
	}


}
