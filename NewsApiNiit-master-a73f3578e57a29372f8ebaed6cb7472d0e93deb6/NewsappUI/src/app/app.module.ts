import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatButtonModule, MatCheckboxModule,MatToolbarModule, MatSidenavModule, MatListModule, MatGridListModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { RegisterComponent } from './modules/authentication/register/register.component';
import { LoginComponent } from './modules/authentication/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import { LayoutModule } from '@angular/cdk/layout';
import {MatTooltipModule} from '@angular/material/tooltip';
import { SearchComponent } from './modules/news/search/search.component';
import { NewsService } from './modules/news/news.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './modules/news/token-interceptor.service';
import { FavouriteComponent } from './modules/news/favourite/favourite.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    SearchComponent,
    FavouriteComponent,
   

  ],
  imports: [
    BrowserModule,
    MatCardModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatTooltipModule,
    FormsModule,
    MatIconModule,
    HttpClientModule,
    MatButtonModule, 
    BrowserAnimationsModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatInputModule,
    LayoutModule,
    MatSidenavModule,
    MatListModule,
    MatGridListModule,
  
  ],
  providers: [NewsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
