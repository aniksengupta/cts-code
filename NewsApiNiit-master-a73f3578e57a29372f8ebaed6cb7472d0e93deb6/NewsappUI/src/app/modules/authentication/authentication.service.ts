import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authServiceEndpoint: string = "http://localhost:8076/userService";
  token: string;

  constructor(private http: HttpClient) { }

  registerUser(newUser) {
    // console.log("from service");
    const url = `${this.authServiceEndpoint}/registerUser`;
    return this.http.post(url, newUser, { responseType: 'text' });
  }

  loginUser(user): Observable<any> {
    const url = `${this.authServiceEndpoint}/login`;
    return this.http.post(url, user);
  }

  setToken(token: string) {
    return localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  deleteToken() {
    return localStorage.removeItem('token');
  }
}
