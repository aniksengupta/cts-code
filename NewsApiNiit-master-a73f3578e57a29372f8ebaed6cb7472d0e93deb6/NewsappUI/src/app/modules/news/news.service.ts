import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from './Article';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  userName: string;
  api_key = "20a484ff97bb4c42b91617727fb0730a";
  url: string = "http://localhost:8086/news/";

  constructor(private http: HttpClient) { }
  initSources() {
    return this.http.get('https://newsapi.org/v2/sources?language=en&apiKey=' + this.api_key);
  }
  initArticles() {
    return this.http.get('https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=' + this.api_key);
  }
  addToFavourite(article) {

    return this.http.post(this.url + "saveNews", article, { responseType: 'text' });
  }
  getMyNews() {

    return this.http.get<Array<Article>>(this.url + "getMyNews");
  }
  deleteFromFavourite(article) {
    return this.http.delete(this.url + "deleteNews/" + article.id);
  }
  searchNews(searchTopic) {
    return this.http.get('https://newsapi.org/v2/everything?q=' + searchTopic + '&apiKey=' + this.api_key);
  }
}
