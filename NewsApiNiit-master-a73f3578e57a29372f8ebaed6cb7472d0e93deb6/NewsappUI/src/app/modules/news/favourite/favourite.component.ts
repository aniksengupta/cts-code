import { Component, OnInit } from '@angular/core';
import { Article } from '../Article';
import { NewsService } from '../news.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {

  articles: Array<Article> = new Array<Article>();
  constructor(private newsService: NewsService, private snackbar: MatSnackBar) { }

  ngOnInit() {

    this.newsService.getMyNews().subscribe(data => {
      this.articles = data;
    });
  }
  deleteFromFavourite(article) {

    for (var i = 0; i < this.articles.length; i++) {
      if (this.articles[i].title === article.title) {
        this.articles.splice(i, 1);
      }
    }
    this.newsService.deleteFromFavourite(article).subscribe((res) => {

      this.snackbar.open('news deleted from favorite', '', {
        duration: 1000
      });
    });
  }

}
