import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { Article } from '../Article';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  mArticles: Array<any>;
  article: Article = new Article();
  constructor(private newsService: NewsService, private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.newsService.initArticles().subscribe(data => this.mArticles = data['articles']);
  }

  AddToFavorite(article) {

    this.newsService.addToFavourite(article).subscribe((article) => {

      this.snackbar.open('news added to favorite', '', {
        duration: 1000
      });
    },
      (error) => {
        this.snackbar.open('news already added to favourite', '', {
          duration: 1000
        });


      });
  }

  onEnter(searchKey) {
    this.newsService.searchNews(searchKey).subscribe(data => this.mArticles = data['articles']);
  }
}





