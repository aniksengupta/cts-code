export class Article {
id: number; 
userId : string;
title : string;
description : string;
publishedAt : string;
sourceWebsiteName : string;
url : string;
urlToImage : string;
}