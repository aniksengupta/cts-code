package com.stackroute.userservice.repositoryTest;

import java.util.Date;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.stackroute.userservice.model.User;
import com.stackroute.userservice.repo.UserRepository;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class UserRepositoryTest {

	@Autowired
	private transient UserRepository userRepository;

	private User user;

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Before
	public void  setUp() {
		user = new User("John", "John Jeny", "Bob", "123456", new Date());
	}
	
	@After
	public void tearDown() {
		System.out.println("from after");
		userRepository.deleteAllInBatch();
	}

	@Test
	public void testRegisterUserSuccess() {
		userRepository.save(user);
		User object = userRepository.findById(user.getUserId()).get();
		assertThat(object.equals(user));
	}
}
