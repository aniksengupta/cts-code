package com.stackroute.userservice.exception;

public class UserNotFoundException extends Exception{
	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public UserNotFoundException(final String message){
		super(message);
		this.message=message;
	}
	@Override
	public String toString() {
		return "UserNotFoundException [message=" + message + "]";
	}

}
