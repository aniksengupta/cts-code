package com.stackroute.userservice.exception;

public class UserAlreadyExistException extends Exception {
	
	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public UserAlreadyExistException(final String message){
		super(message);
		this.message=message;
	}
	
	@Override
	public String toString() {
		return "UserAlreadyExistException [message=" + message + "]";
	}

	

}
