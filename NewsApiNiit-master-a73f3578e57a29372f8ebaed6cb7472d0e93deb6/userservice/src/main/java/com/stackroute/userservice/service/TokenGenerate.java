package com.stackroute.userservice.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.stackroute.userservice.model.User;



@Service
public interface TokenGenerate {
	Map<String,String> generateToken(User user);
}
