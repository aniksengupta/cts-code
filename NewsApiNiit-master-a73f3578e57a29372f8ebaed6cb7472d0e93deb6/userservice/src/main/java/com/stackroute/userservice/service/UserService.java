package com.stackroute.userservice.service;

import org.springframework.stereotype.Service;


import com.stackroute.userservice.exception.UserAlreadyExistException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;



@Service
public interface UserService {

	boolean saveUser(User user) throws UserAlreadyExistException;
	User findByUserIdAndPassword(String userId,String password) throws UserNotFoundException;
}
