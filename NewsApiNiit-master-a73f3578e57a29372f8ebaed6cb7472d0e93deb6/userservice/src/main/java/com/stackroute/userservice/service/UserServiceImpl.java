package com.stackroute.userservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.stackroute.userservice.exception.UserAlreadyExistException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;
import com.stackroute.userservice.repo.UserRepository;



@Service
public class UserServiceImpl implements UserService {
	
	@Autowired 
	private UserRepository userRepo;

	@Override
	public boolean saveUser(User user) throws UserAlreadyExistException {
		// TODO Auto-generated method stub
		
		Optional<User> existingUser=userRepo.findById(user.getUserId());
		if(existingUser.isPresent()){
			throw new UserAlreadyExistException("user already exists");
		}
		userRepo.save(user);
		return true;
	}

	@Override
	public User findByUserIdAndPassword(String userId, String password) throws UserNotFoundException {
		// TODO Auto-generated method stub
		User user=userRepo.findByUserIdAndPassword(userId, password);
		if(user==null)
		{
			throw new UserNotFoundException("user not found");
		}
		return user;
	}
	

}
