package com.stackroute.userservice.controller;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stackroute.userservice.exception.UserAlreadyExistException;
import com.stackroute.userservice.model.User;
import com.stackroute.userservice.service.TokenGenerate;
import com.stackroute.userservice.service.UserService;



@RestController
@RequestMapping("/userService")
@CrossOrigin()
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenGenerate tokenGenerator;
	
	@PostMapping("/registerUser")
	public ResponseEntity<?> registerUser(@RequestBody User user){
		System.out.println(user.getUserId());
		ResponseEntity<?> responseEntity;
		try{
			System.out.println(user.getUserId());
			userService.saveUser(user);
			responseEntity= new ResponseEntity<String>("user registered",HttpStatus.CREATED);
		}
		catch(UserAlreadyExistException ex){
			responseEntity= new ResponseEntity<String>(ex.getMessage(),HttpStatus.CONFLICT);
		}
		
		return responseEntity;
	}
	
	@PostMapping("login")
	public ResponseEntity<?> login(@RequestBody User loginDetail) {
		try {
			if (null == loginDetail.getUserId() || null == loginDetail.getPassword()) {
				throw new Exception("User Id or Password canot be empty.");
			}
			User user = userService.findByUserIdAndPassword(loginDetail.getUserId(), loginDetail.getPassword());
			Map<String, String> map = tokenGenerator.generateToken(user);
			return new ResponseEntity<Map<String, String>>(map, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		}
	}

}
