//package com.example.Mapping.controller;
//
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.example.Mapping.Entity.Contact;
//import com.example.Mapping.Entity.User;
//import com.example.Mapping.repository.UserRepository;
//
//@RestController
//public class MappingController {
//	
//	@Autowired
//	UserRepository ur;
//	
//	@GetMapping("users/getAll")
//	public List<User> getAll(){
//		
//		System.out.println("inside getAll");
//		User user = new User();
//		user.setName("User1");
//		
//		Contact c = new Contact();
//		Contact c2 = new Contact();
//		
//		c.setNumber("123456");
//		c2.setNumber("123456");
//		
//		Set<Contact> s = new HashSet<>();
//		s.add(c);
//		s.add(c2);
//		
//		user.setContacts(s);
//		
//		System.out.println("val = "+user.toString());
//		
//		ur.save(user);
//		return (List<User>) ur.findAll();
//	}
//
//}
