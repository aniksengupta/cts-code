package com.example.Mapping.Entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;



@Component
@Entity
@Table(name="Users")
public class Users {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="User_ID")
	int userId;
	
	String name;
	
	@OneToMany(mappedBy="uc",cascade=CascadeType.ALL)
	Set<Contacts> contacts ;
	

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Contacts> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contacts> contacts) {
		this.contacts = contacts;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", name=" + name + ", contacts=" + contacts + "]";
	}
	
	
	
}
