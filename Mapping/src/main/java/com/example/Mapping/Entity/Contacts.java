package com.example.Mapping.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="Contacts")
public class Contacts {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="contact_id")
	int id;
	
	@Column(name="contact_number")
	String number;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="User_ID")
	private Users uc;
	
	public int getId() {
		return id;
	}

	
	public Users getUc() {
		return uc;
	}


	public void setUc(Users uc) {
		this.uc = uc;
	}


	public void setId(int id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}


}
