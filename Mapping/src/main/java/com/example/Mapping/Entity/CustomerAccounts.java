package com.example.Mapping.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Customer_Accounts")
public class CustomerAccounts {
	
	
	@Id
	@Column
	int id;
	
	
	@Column(name="Customer_Name")
	String customerName;
	
	
	
	 @ManyToOne(fetch=FetchType.LAZY)
     @JoinColumn(name = "Associate_ID" , insertable = false, updatable = false)
		     Associates cpID;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	public Associates getCpID() {
		return cpID;
	}


	public void setCpID(Associates cpID) {
		this.cpID = cpID;
	}

	 
	 
}
