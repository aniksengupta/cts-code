package com.example.Mapping.Entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name="Associates")
public class Associates {
	

	@Id
	@Column(name="Associate_ID") 
	int associateID;	
	
	String assoName;
    
 
    @OneToMany(mappedBy="cpID",cascade=CascadeType.ALL)
    Set<CustomerAccounts>   customerAccounts1;


	public int getAssociateID() {
		return associateID;
	}


	public void setAssociateID(int associateID) {
		this.associateID = associateID;
	}


	public String getAssoName() {
		return assoName;
	}


	public void setAssoName(String assoName) {
		this.assoName = assoName;
	}


	public Set<CustomerAccounts> getCustomerAccounts1() {
		return customerAccounts1;
	}


	public void setCustomerAccounts1(Set<CustomerAccounts> customerAccounts1) {
		this.customerAccounts1 = customerAccounts1;
	}
    
  
}
