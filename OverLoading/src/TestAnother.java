// example of overloading eith type promotion 
// https://beginnersbook.com/2013/05/method-overloading/ -- go to this link


// byte-short-int-long-float-double

public class TestAnother {
		
	void add(int a,int b) {
		System.out.println("method A --> result  = "+(a+b));
		
	}
	
	void add(int a,double b) {
		System.out.println("method B ---> result = "+(a+b));
	}
}
