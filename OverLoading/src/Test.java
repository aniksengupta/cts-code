
public class Test {
	
	public void add() {
		System.out.println("void type with no parameter");
	}
	
	public int add(int a,int b) {
		System.out.println("int type with 2 parameter ");
		return (a+b);
	}
	
	public Double add(Double a,int b) {
		System.out.println("double type ");
		return (a+b);
	}
	
	public int add(int a,int b,int c) {
		System.out.println("from type int 3 parameters");
		return (a+b+c);
	}
	
}
