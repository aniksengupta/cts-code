
public class ConstructorChaining {
	
	public ConstructorChaining() {
		this(2);
		System.out.println("default");
	}
	
	public ConstructorChaining(int i) {
		this(2,3);
		System.out.println("with 1 parameter");
		
	}
	
	public ConstructorChaining(int i,int j) {
		System.out.println("with 2 parameter");
	}

}
