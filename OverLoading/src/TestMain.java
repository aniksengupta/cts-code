
public class TestMain {

	public static void main (String args[]) {
		
		Test t1 = new Test();
		t1.add();
		t1.add(2,3);
		t1.add(2,3,4);
		t1.add(10.0,2);
		
		// type promotion
		
		TestAnother obj = new TestAnother();
		obj.add(2, 5);
		obj.add(1,5.0f);
		
		ConstructorChaining cc = new ConstructorChaining();
	}
	
}
